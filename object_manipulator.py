import bpy

from . import utils
import math


class ObjectManipulator:
    def __init__(self):
        self.scene = bpy.context.scene
        self.utls = utils.Utils()

    def set_all_object_origin_to_geometry(self):
        candidate_list = [item.name for item in bpy.data.objects
                          if item.type == "MESH" and
                          utils.SPAWNED_OBJ_POSTFIX in item.name and
                          utils.WITH_CONE_POSTFIX not in item.name]
        for obj in candidate_list:
            bpy.data.objects[obj].select_set(True)
            bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')

    def randomize_all_objects_position_in_area(self, origin, diagonal):
        for obj in self.scene.objects:
            if utils.SPAWNED_OBJ_POSTFIX in obj.name:
                obj.location.x = self.utls.rand_float(origin.x, diagonal.x)
                obj.location.y = self.utls.rand_float(origin.y, diagonal.y)
                obj.location.z = self.utls.rand_float(origin.z, diagonal.z)

    def set_object_position(self, handle, x, y, z):
        handle.location.x = x
        handle.location.y = y
        handle.location.z = z

    def set_object_dimensions(self, handle, x, y, z):
        handle.dimensions = [x, y, z]

    def set_random_object_rotation(self, handle):
        handle.rotation_euler = [self.utls.rand_float(0, 2 * math.pi),
                                 self.utls.rand_float(0, 2 * math.pi),
                                 self.utls.rand_float(0, 2 * math.pi)]

    def set_random_object_size(self, handle, scale_factor):

        handle.scale = [handle.scale[0] * scale_factor,
                        handle.scale[1] * scale_factor,
                        handle.scale[2] * scale_factor]

    def delete_cone_vertex_group_from_containers(self):
        candidate_list = [item.name for item in bpy.data.objects
                          if item.type == "MESH" and
                          utils.SPAWNED_OBJ_POSTFIX in item.name and
                          utils.WITH_CONE_POSTFIX in item.name]
        print("------ITEMS WITH CONE-------")
        print(candidate_list)
        print("----------------------------")
        scene = bpy.context.scene
        for obj in candidate_list:
            bpy.ops.object.mode_set(mode='OBJECT')
            obj = scene.objects[obj]
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(type="VERT")
            bpy.ops.mesh.select_all(action='DESELECT')
            bpy.ops.object.mode_set(mode='OBJECT')
            for vert in obj.data.vertices:
                if vert.co.y >= 0.1:
                    vert.select = True
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.delete(type="VERT")
        if candidate_list:
            bpy.ops.object.mode_set(mode='OBJECT')

    def randomize_all_object_rotation(self):
        for obj in self.scene.objects:
            if utils.DONT_RANDOMIZE_POS in obj.name or utils.WITH_CONE_POSTFIX in obj.name:
                continue
            if utils.SPAWNED_OBJ_POSTFIX in obj.name:
                self.set_random_object_rotation(obj)

    def randomize_aselected_object_size(self):

        for obj in self.scene.objects:
            if utils.DONT_RANDOMIZE_SIZE in obj.name:
                continue
            if utils.SPAWNED_OBJ_POSTFIX in obj.name and any(name in obj.name for name in utils.CLASSES_TO_RAND_SIZE_DOWN):
                print("randomizing size of ", obj.name)
                scale_factor = self.utls.rand_float(0.7, 1.0)
                self.set_random_object_size(obj, scale_factor)
            elif utils.SPAWNED_OBJ_POSTFIX in obj.name and any(name in obj.name for name in utils.CLASSES_TO_RAND_SIZE_UP):
                print("randomizing size of ", obj.name)
                scale_factor = self.utls.rand_float(0.85, 1.15)
                self.set_random_object_size(obj, scale_factor)

    def stop_object_movement(self, stop=True):
        # candidate_list = [item.name for item in bpy.data.objects if
        #                   item.type == "MESH" and SPAWNED_OBJ_POSTFIX in item.name]
        # for obj in candidate_list:
        #     bpy.data.objects[obj].select_set(True)
        #     bpy.context.view_layer.objects.active = bpy.data.objects[obj]
        #     bpy.context.object.rigid_body.enabled = not stop
        for obj in self.scene.objects:
            if obj.type == 'MESH' and utils.SPAWNED_OBJ_POSTFIX in obj.name:
                obj.select_set(True)
                obj.rigid_body.enabled = not stop

    def delete_meshes(self):
        for block in bpy.data.meshes:
            if block.users == 0:
                bpy.data.meshes.remove(block)

        for block in bpy.data.materials:
            if block.users == 0:
                bpy.data.materials.remove(block)

        for block in bpy.data.textures:
            if block.users == 0:
                bpy.data.textures.remove(block)

        for block in bpy.data.images:
            if block.users == 0:
                bpy.data.images.remove(block)
