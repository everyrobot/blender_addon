import bpy

from . import utils


class PhysicsManipulator:
    def __init__(self):
        self.scene = bpy.context.scene

    def set_friction_for_all_objects(self, friction):
        scene = bpy.context.scene
        for obj in scene.objects:
            if obj.type == 'MESH' and utils.SPAWNED_OBJ_POSTFIX in obj.name:
                obj.rigid_body.friction = friction

    def set_angular_damping(self, damping):
        scene = bpy.context.scene
        for obj in scene.objects:
            if obj.type == 'MESH' and utils.SPAWNED_OBJ_POSTFIX in obj.name:
                obj.rigid_body.angular_damping = damping

    def set_rigid_body_world_settings(self):
        self.scene.rigidbody_world.steps_per_second = 120
        self.scene.rigidbody_world.solver_iterations = 40

    def set_all_objects_sticky_surface(self):
        candidate_list = [item.name for item in bpy.data.objects if item.type == "MESH" and
                          utils.SPAWNED_OBJ_POSTFIX in item.name]

        for x in candidate_list:
            bpy.data.objects[x].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[x]
            bpy.context.object.rigid_body.friction = 1.0
            bpy.context.object.rigid_body.angular_damping = 1.0

    def set_containers_properties(self):
        candidate_list = [item.name for item in bpy.data.objects if item.type == "MESH" and
                          utils.SPAWNED_OBJ_POSTFIX in item.name and
                          utils.WITH_CONE_POSTFIX in item.name]
        print("CANDIDATE LIST ", candidate_list)
        for x in candidate_list:
            bpy.data.objects[x].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[x]
            bpy.ops.rigidbody.object_add()
            bpy.context.object.rigid_body.collision_shape = 'MESH'
            bpy.context.object.rigid_body.use_margin = True
            bpy.context.object.rigid_body.collision_margin = 0.001
            bpy.context.object.rigid_body.enabled = False

    def set_all_object_rigid_body_and_collision(self, with_containers=True):
        candidate_list = [item.name for item in bpy.data.objects
                          if item.type == "MESH" and
                          utils.SPAWNED_OBJ_POSTFIX in item.name]

        if not with_containers:
            candidate_list = [name for name in candidate_list if utils.WITH_CONE_POSTFIX not in name]

        for obj in candidate_list:
            bpy.data.objects[obj].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[obj]
            bpy.ops.rigidbody.object_add()
            bpy.context.object.rigid_body.collision_shape = 'CONVEX_HULL'
            bpy.context.object.rigid_body.use_margin = True
            bpy.context.object.rigid_body.collision_margin = 0.001
