import bpy
import glob
import os
import sys
import random
import csv
from operator import add
import mathutils
import math
import colorsys
import re


class Robot:
    def __init__(self, cam_name='cam_1', offset=None,
                 poses_file_path="/home/"+ os.environ.get('USERNAME') +"/.config/blender/2.90/scripts/addons/blender_addon/robot_files/franka_robot/",
                 robot_parts_path="/home/"+ os.environ.get('USERNAME') +"/.config/blender/2.90/scripts/addons/blender_addon/robot_files/franka_robot/"):

        if cam_name is 'cam_1':
            if offset is None:
                self.offset = {'x': 0.23, 'y': 1.0, 'z': 0.0}
            else:
                self.offset = offset
        else:
            #TODO: add default offsets for cam 2
            if offset is None:
                self.offset = {'x': -0.3, 'y': 0.23, 'z': 0.0}
            else:
                self.offset = offset

        self.robot_parts_path = robot_parts_path
        self.poses_file_path = os.path.join(poses_file_path, "positions_franka_cam_1.csv")
        self.robot_parts_files = glob.glob(os.path.join(self.robot_parts_path, "*.obj"))
        self.robot_parts_files = sorted(self.robot_parts_files, key=lambda x: int(x.split('/')[-1].split('.')[0][4:]))
        self.amount_of_robot_parts = len(self.robot_parts_files)
        print(self.amount_of_robot_parts)
        self.link_poses = None
        self.base_link_pose = [-0.1200003027916 + self.offset['x'], 1.5950237752804e-07 + self.offset['y'],
                               0.079999983310699 + self.offset['z']] # cam_1
        # self.base_link_pose = [-1.6763806343079e-07 + self.offset['x'], -0.12000029534101 + self.offset['y'],
        #                        0.079999923706055 + self.offset['z']] # cam_2
        self.robot_parts_names = list()

        for i in range(0, len(self.robot_parts_files)):
            bpy.ops.object.select_all(action='DESELECT')
            bpy.ops.import_scene.obj(filepath=self.robot_parts_files[i])
            imported_objects = bpy.context.selected_objects
            imported_objects[0].name = 'Link' + str(i + 1)
            self.robot_parts_names.append(imported_objects[0].name)

        scene = bpy.context.scene
        for link_name in self.robot_parts_names:
            bpy.ops.object.select_all(action='DESELECT')
            obj = scene.objects[link_name]
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
            bpy.ops.mesh.customdata_custom_splitnormals_clear()
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.mark_sharp(clear=True)
            bpy.ops.mesh.faces_shade_smooth()
            bpy.ops.object.mode_set(mode='OBJECT')
            obj.select_set(False)
            bpy.data.meshes[link_name].use_auto_smooth = True
        self.robot_name = 'Link11'
        self.robot_segmentation_name = 'FrankaArm'
        self.links_names = None
        self.base_name = self.robot_parts_names[0]
        self.read_poses()

    def read_poses(self):

        with open(self.poses_file_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            line_count = 0
            init_line = True
            all_pos_bundles = list()
            current_pos = list()
            for row in csv_reader:

                link_pos = [row['x'], row['y'], row['z'], row['qx'], row['qy'], row['qz'], row['qw']]
                current_pos.append(link_pos)
                line_count += 1
                if line_count == 10:
                    all_pos_bundles.append(current_pos)
                    current_pos = list()
                    line_count = 0
            self.link_poses = all_pos_bundles

    def shift_robot_pose(self):
        target_position = random.choice(self.link_poses)
        scene = bpy.context.scene
        self.links_names = [link_name.name for link_name in bpy.data.objects if
                            any(name in link_name.name for name in self.robot_parts_names)]
        self.links_names = sorted(self.links_names, key=lambda x: int(x[4:]))
        base_name = self.links_names[0]
        links_names = self.links_names[1:]
        link_pose_list = list(map(lambda n, p: (n, p), links_names, target_position))
        base = scene.objects[base_name]
        base.location = tuple([float(value) for value in self.base_link_pose])
        base.rotation_mode = 'QUATERNION'
        base_quat = (0.70710670948029, 0, 0, -0.70710682868958) #cam_2
        # base_quat = (0, 0, 0, -1)
        base.rotation_quaternion = base_quat

        for pair in link_pose_list:
            obj = scene.objects[pair[0]]
            obj.location = tuple(list(map(add, [float(value) for value in pair[1][:3]],
                                          [self.offset['x'], self.offset['y'], self.offset['z']])))
            obj.rotation_mode = 'QUATERNION'
            quat = [pair[1][-1]] + list(pair[1][3:6])
            obj.rotation_quaternion = tuple([float(value) for value in quat])

        # self.set_robot_material(self.links_names)
        self.join_robot(False)
        self.join_gripper(False)
        self.change_robot_name(self.robot_segmentation_name)
        self.join_gripper_with_robot()
        # obj = scene.objects['Link11']
        # obj_rotation = obj.matrix_world.to_euler()
        # obj_rotation[-1] += math.radians(180)
        # # obj = scene.objects['Link11']
        # obj.rotation_mode = 'XYZ'
        # obj.rotation_euler = obj_rotation

    def join_gripper_with_robot(self):
        bpy.ops.object.select_all(action='DESELECT')
        scene = bpy.context.scene
        franka_arm_name = [item.name for item in bpy.data.objects if re.match('[a-zA-Z]{4}\d{1,2}', item.name)][0]
        obj = scene.objects[franka_arm_name]
        obj.select_set(True)
        grp = scene.objects[self.robot_segmentation_name]
        grp.select_set(True)
        bpy.context.view_layer.objects.active = grp
        bpy.ops.object.join()


    def join_robot(self, set_new_mat):
        bpy.ops.object.select_all(action='DESELECT')
        scene = bpy.context.scene
        if set_new_mat:
            mat = bpy.data.materials.get("avena_robot_material")
            color = colorsys.hsv_to_rgb(0.0, 0.0, random.uniform(0.16, 0.7))
            color = self.srgb_to_linear(*color)
        for link_name in self.links_names[:8]:
            obj = scene.objects[link_name]
            obj.select_set(True)
            if set_new_mat:
                self.set_material(obj, mat, (*color, 1), "avena_gripper_material")
            bpy.context.view_layer.objects.active = obj
        bpy.ops.object.join()

    def join_gripper(self, set_new_mat):
        bpy.ops.object.select_all(action='DESELECT')
        scene = bpy.context.scene
        if set_new_mat:
            mat = bpy.data.materials.get("avena_gripper_material")
            color = colorsys.hsv_to_rgb(0.0, 0.0, random.uniform(0.16, 0.7))
            color = self.srgb_to_linear(*color)
        for link_name in self.links_names[8:]:
            obj = scene.objects[link_name]
            obj.select_set(True)
            if set_new_mat:
                self.set_material(obj, mat, (*color, 1), "avena_gripper_material")
            bpy.context.view_layer.objects.active = obj
        bpy.ops.object.join()

    def change_robot_name(self, name):
        scene = bpy.context.scene
        obj = scene.objects[self.robot_name]
        obj.name = name

    def set_material(self, obj, mat, color_tuple, mat_name):
        if mat is None:
            # create material
            mat = bpy.data.materials.new(name=mat_name)
            mat.diffuse_color = color_tuple
            # Assign it to object
        if obj.data.materials:
            # assign to 1st material slot
            obj.data.materials[0] = mat
        else:
            # no slots
            obj.data.materials.append(mat)

    def srgb_to_linear(self, r, g, b):
        def srgb(c):
            a = .055
            if c <= .04045:
                return c / 12.92
            else:
                return ((c + a) / (1 + a)) ** 2.4
        return tuple(srgb(c) for c in (r, g, b))
