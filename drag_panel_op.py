import subprocess
from .bl_ui_label import *
from .bl_ui_button import *
from .bl_ui_checkbox import *
from .bl_ui_slider import *
from .bl_ui_textbox import *
from .bl_ui_drag_panel import *
from .bl_ui_draw_op import *
from . import spawner
from . import utils
from . import dataset_creator
from . import policies_enum
from . import Policies
import os

# import pydevd_pycharm
# pydevd_pycharm.settrace('localhost', port=1090, stdoutToServer=True, stderrToServer=True)

# import time
WIDGET_INSTANCE = None
ADDON_DIR_NAME = 'blender_addon'

class DP_OT_draw_operator(BL_UI_OT_draw_operator):
    bl_idname = "object.dp_ot_draw_operator"
    bl_label = "bl ui widgets custom operator"
    bl_description = "Demo operator for bl ui widgets"
    bl_options = {'REGISTER'}

    def __init__(self):
        global WIDGET_INSTANCE
        super().__init__()
        # origin = utils.Point(-1.5, -1.5, 0)
        # diagonal = utils.Point(1.5, 1.5, 1)
        #self.spn = spawner.Spawner(origin, diagonal)
        self.enabled_policy_buttons = list()
        self.config_file_path = ''
        self.models_dir_path = ''
        self.output_dir_path = ''
        self.classes_to_check_for_check = dict()
        self.lower_widget_bound = 330
        self.buttons_start_x = 70
        self.buttons_start_y = 30
        self.header_x_start = self.buttons_start_x
        self.box_y_start = self.buttons_start_y + 150
        self.origin = utils.Area.get_arena_from_file().origin
        self.diagonal = utils.Area.get_arena_from_file().diagonal
        self.arena = utils.Area(origin=self.origin, diagonal=self.diagonal)
        self.lower_bound_panel_height_diff = 150
        self.total_samples_to_generate = 0
        self.state_saved = dict()
        self.pics_per_policy_dict = dict()
        ###########################################################

        self.panel = BL_UI_Drag_Panel(100, 300, 785, self.lower_widget_bound)
        self.panel.bg_color = (0.2, 0.2, 0.2, 0.9)

        self.header = BL_UI_Label(self.header_x_start, self.buttons_start_y, 90, 15)
        self.header.text = 'AVENA SYNTHETIC DATASET CREATOR'
        self.header.text_size = 20
        self.header.text_color = (0.2, 0.9, 0.9, 1.0)

        self.avena_logo = BL_UI_Button(self.buttons_start_x + 400, self.buttons_start_y, 0, 0)
        self.avena_logo.disabled = True
        self.avena_logo.bg_color = (0.2, 0.2, 0.2, 0.9)
        self.avena_logo.hover_bg_color = (0.2, 0.2, 0.2, 0.9)
        self.avena_logo.text = ""
        self.logo_path = os.path.join('/home', os.environ.get('USER'), '.config', 'blender', '2.90', 'scripts',
                                      'addons', ADDON_DIR_NAME, 'avena_logo_2020_full_800px.png')

        self.avena_logo.set_image(self.logo_path)
        self.avena_logo.set_image_size((202, 60))
        self.avena_logo.set_image_position((4, 2))
        # self.avena_logo.set_mouse_down(self.on_features_num_approved)

        self.copyright = BL_UI_Label(self.buttons_start_x + 620, self.buttons_start_y, 50, 15)
        self.copyright.text = "(c) Avena Robotics Inc."
        self.copyright.text_size = 8
        self.copyright.text_color = (0.5, 0.5, 0.5, 1.0)

        self.www = BL_UI_Button(self.buttons_start_x + 610, self.buttons_start_y + 15, 100, 15)
        self.www.disabled = False
        self.www.bg_color = (0.2, 0.2, 0.2, 0.9)
        self.www.hover_bg_color = (0.2, 0.2, 0.2, 0.9)
        self.select_bg_color = (0.2, 0.2, 0.2, 0.9)
        self.www.text = "www.avena.world"
        # self.button2.set_image("//img/rotate.png")
        self.www.set_image_size((24, 24))
        self.www.set_image_position((4, 2))
        self.www.set_mouse_down(self.on_www_clicked)

        self.label = BL_UI_Label(self.buttons_start_x + 90, self.lower_widget_bound - 40, 90, 15)
        self.label.text = "Samples created:"
        self.label.text_size = 14
        self.label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.iter_label = BL_UI_Label(self.buttons_start_x + 240, self.lower_widget_bound - 40, 50, 15)
        self.iter_label.text = '0 of 0'
        self.iter_label.text_size = 14
        self.iter_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.max_features = BL_UI_Label(self.buttons_start_x + 230, self.buttons_start_y + 30, 40, 15)
        self.max_features.text = ''
        self.max_features.text_size = 14
        self.max_features.text_color = (0.2, 0.9, 0.9, 1.0)

        self.button1 = BL_UI_Button(self.buttons_start_x, self.lower_widget_bound - 120, 120 + 67, 30)
        self.button1.disabled = True
        self.button1.bg_color = (0.2, 0.8, 0.8, 0.8)
        self.button1.hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        self.button1.text = "Start dataset generation"
        # self.button1.set_image("//img/scale.png")
        self.button1.set_image_size((24, 24))
        self.button1.set_image_position((4, 2))
        self.button1.set_mouse_down(self.button1_press)

        self.button2 = BL_UI_Button(self.buttons_start_x + 207, self.lower_widget_bound - 120, 120 + 67, 30)
        self.button2.disabled = True
        self.button2.bg_color = (0.2, 0.8, 0.8, 0.8)
        self.button2.hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        self.button2.text = "Stop and clear scene"
        # self.button2.set_image("//img/rotate.png")
        self.button2.set_image_size((24, 24))
        self.button2.set_image_position((4, 2))
        self.button2.set_mouse_down(self.button2_press)

        self.goto_results_button = BL_UI_Button(self.buttons_start_x, self.lower_widget_bound - 80, 260 + 134, 30)
        self.goto_results_button.disabled = True
        self.goto_results_button.bg_color = (0.2, 0.8, 0.8, 0.8)
        self.goto_results_button.hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        self.goto_results_button.text = "Open results dir"
        # self.button2.set_image("//img/rotate.png")
        self.goto_results_button.set_image_size((24, 24))
        self.goto_results_button.set_image_position((4, 2))
        self.goto_results_button.set_mouse_down(self.goto_results_button_press)
        #############################################################################
        # STEP LABELS

        self.step_one = BL_UI_Label(20, self.buttons_start_y + 73, 90, 15)
        self.step_one.text = 'Step 1'
        self.step_one.visible = True
        self.step_one.text_size = 12
        self.step_one.text_color = (1.0, 1.0, 1.0, 1.0)

        self.step_two = BL_UI_Label(20, self.buttons_start_y + 113, 90, 15)
        self.step_two.text = 'Step 2'
        self.step_two.visible = False
        self.step_two.text_size = 12
        self.step_two.text_color = (1.0, 1.0, 1.0, 1.0)

        self.step_three = BL_UI_Label(20, self.buttons_start_y + 150, 90, 15)
        self.step_three.text = 'Step 3'
        self.step_three.visible = False
        self.step_three.text_size = 12
        self.step_three.text_color = (1.0, 1.0, 1.0, 1.0)

        #############################################################################
        # step 4 description

        self.step_three_description = BL_UI_Label(self.buttons_start_x, self.buttons_start_y + 150, 90, 15)
        self.step_three_description.text = 'Select object spawning policies to use:'
        self.step_three_description.visible = False
        self.step_three_description.text_size = 14
        self.step_three_description.text_color = (0.2, 0.9, 0.9, 1.0)

        self.models_path_button = BL_UI_Button(self.buttons_start_x, self.buttons_start_y + 70, 120, 30)
        self.models_path_button.disabled = False
        self.models_path_button.bg_color = (0.2, 0.8, 0.8, 0.8)
        self.models_path_button.hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        self.models_path_button.text = "Select models dir"
        # self.button2.set_image("//img/rotate.png")
        self.models_path_button.set_image_size((24, 24))
        self.models_path_button.set_image_position((4, 2))
        self.models_path_button.set_mouse_down(self.models_path_button_press)

        self.models_path_label = BL_UI_Label(self.buttons_start_x + 150, self.buttons_start_y + 73, 90, 15)
        self.models_path_label.text = 'None'
        self.models_path_label.text_size = 14
        self.models_path_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.output_path_button = BL_UI_Button(self.buttons_start_x, self.buttons_start_y + 110, 120, 30)
        self.output_path_button.disabled = True
        self.output_path_button.bg_color = (0.2, 0.8, 0.8, 0.8)
        self.output_path_button.hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        self.output_path_button.text = "Select output dir"
        # self.button2.set_image("//img/rotate.png")
        self.output_path_button.set_image_size((24, 24))
        self.output_path_button.set_image_position((4, 2))
        self.output_path_button.set_mouse_down(self.output_path_button_press)

        self.output_path_label = BL_UI_Label(self.buttons_start_x + 150, self.buttons_start_y + 113, 90, 15)
        self.output_path_label.text = 'None'
        self.output_path_label.text_size = 14
        self.output_path_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.applied_policies_label = BL_UI_Label(self.buttons_start_x + 150, self.buttons_start_y + 113, 90, 15)
        self.applied_policies_label.text = ''
        self.applied_policies_label.text_size = 14
        self.applied_policies_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.applied_policies_label = BL_UI_Label(0, 0, 90, 15)
        self.applied_policies_label.text = ''
        self.applied_policies_label.text_size = 14
        self.applied_policies_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.generate_masks_box = BL_UI_Checkbox(self.buttons_start_x, self.lower_widget_bound + 300, 100, 15)
        self.generate_masks_box.text = 'Generate masks'
        self.generate_masks_box.text_size = 14
        self.generate_masks_box.text_color = (0.2, 0.9, 0.9, 1.0)
        self.generate_masks_box.enabled = True
        self.generate_masks_box.state = utils.RENDER_MASKS
        self.generate_masks_box.set_toggle_action(self.toggle_masks_render)

        self.generate_depth_box = BL_UI_Checkbox(self.buttons_start_x, self.lower_widget_bound + 275, 100, 15)
        self.generate_depth_box.text = 'Generate depth'
        self.generate_depth_box.text_size = 14
        self.generate_depth_box.text_color = (0.2, 0.9, 0.9, 1.0)
        self.generate_depth_box.enabled = True
        self.generate_depth_box.state = utils.GENERATE_DEPTH
        self.generate_depth_box.set_toggle_action(self.toggle_depth_render)

        self.policies_dict = dict()
        WIDGET_INSTANCE = self

    def toggle_masks_render(self, widget):
        if utils.RENDER_MASKS:
            utils.RENDER_MASKS = False
        else:
            utils.RENDER_MASKS = True
        print('RENDERING MASKS: ', utils.RENDER_MASKS)

    def toggle_depth_render(self, widget):
        if utils.GENERATE_DEPTH:
            utils.GENERATE_DEPTH = False
        else:
            utils.GENERATE_DEPTH = True
        print('RENDERING DEPTH: ', utils.GENERATE_DEPTH)

    def save_application_state(self):
        main_panel_arrts = dir(self)
        for attr in main_panel_arrts:
            if type(getattr(self, attr)) == BL_UI_Button:
                if getattr(self, attr).with_checkbox:
                    self.state_saved[attr] = {'button_disabled': getattr(self, attr).disabled,
                                              'checkbox_enabled': getattr(self, attr).checkbox.enabled}
                else:
                    self.state_saved[attr] = getattr(self, attr).disabled
            elif type(getattr(self, attr)) == BL_UI_Checkbox:
                self.state_saved[attr] = getattr(self, attr).enabled
        pass

    def load_saved_application_state(self):
        for attr in self.state_saved:
            if type(getattr(self, attr)) == BL_UI_Button:
                if getattr(self, attr).with_checkbox:
                    getattr(self, attr).disabled = self.state_saved[attr]['button_disabled']
                    getattr(self, attr).checkbox.enabled = self.state_saved[attr]['checkbox_enabled']

                else:
                    getattr(self, attr).disabled = self.state_saved[attr]

            elif type(getattr(self, attr)) == BL_UI_Checkbox:
                getattr(self, attr).enabled = self.state_saved[attr]

        pass

    def create_poilcies_dict(self):
        for policy in policies_enum.PoliciesEnum:
            self.policies_dict[str(policy.name)] = {
                "instance": None
            }

    def get_base_widget_list(self):
        widgets_panel = [self.header, self.step_one, self.step_two,
                         self.step_three,
                         self.step_three_description, self.goto_results_button, self.models_path_label, self.www,
                         self.output_path_label, self.output_path_button, self.avena_logo, self.copyright,
                         self.models_path_button, self.max_features, self.label,
                         self.button1, self.button2, self.iter_label, self.applied_policies_label]

        return widgets_panel

    @staticmethod
    def enable_policy_buttons_checkboxes(obj):
        attr_list = dir(obj)
        for attr in attr_list:
            if type(getattr(obj, attr)) == BL_UI_Button:
                if getattr(obj, attr).with_checkbox:
                    if getattr(obj, attr).disabled:
                        if attr in obj.enabled_policy_buttons:
                            getattr(obj, attr).disabled = False
                    getattr(obj, attr).checkbox.enabled = True
        obj.enabled_policy_buttons = list()

    @staticmethod
    def disable_policy_buttons_checkboxes(obj):
        attr_list = dir(obj)
        for attr in attr_list:
            if type(getattr(obj, attr)) == BL_UI_Button:
                if getattr(obj, attr).with_checkbox:
                    if not getattr(obj, attr).disabled:
                        obj.enabled_policy_buttons.append(attr)
                        getattr(obj, attr).disabled = True
                    getattr(obj, attr).checkbox.enabled = False

    def spawn_policy_buttons(self):

        widgets_panel = self.get_base_widget_list()
        policy_buttons_y_start = self.buttons_start_y + 150
        curr_policy_button_no = 1
        counter = 0
        for name, value in policies_enum.PoliciesEnum.__members__.items():

            # button
            setattr(self, name,
                    BL_UI_Button(self.buttons_start_x + 40, policy_buttons_y_start + curr_policy_button_no * 40, 120,
                                 30))
            # set an attribute to hold policy object instance
            setattr(self, name + '_instance', None)
            getattr(self, name).disabled = True
            getattr(self, name).bg_color = (0.2, 0.8, 0.8, 0.8)
            getattr(self, name).hover_bg_color = (0.2, 0.9, 0.9, 1.0)
            getattr(self, name).text = name
            getattr(self, name).set_image_size((24, 24))
            getattr(self, name).set_image_position((4, 2))
            ############### TODO: ALWAYS TRUE WHEN NOT PRESENTING TO BOSS
            getattr(self, name)._with_checkbox = True if counter < 3 else False
            getattr(self, name).set_mouse_down(self.on_click_policy_button)
            getattr(self, name).set_checkbox_unchecked_action(self.uncheck_policy_button_box_callback)
            getattr(self, name).set_checkbox_checked_action(self.check_policy_button_box_callback)
            widgets_panel.append(getattr(self, name))
            curr_policy_button_no += 1
            counter += 1
        self.lower_widget_bound += curr_policy_button_no * 30
        self.panel.height = self.lower_widget_bound + curr_policy_button_no * 25 - 50
        self.spawn_bottom_buttons(self.lower_widget_bound)
        widgets = [self.panel]
        widgets += widgets_panel

        self.init_widgets(bpy.context, widgets)
        self.panel.add_widgets(widgets_panel)
        pass

    def get_policy_buttons_attr_names(self):
        ret_buttons_sttr_list = list()
        # redrawing the existing policy buttons
        for name, value in policies_enum.PoliciesEnum.__members__.items():
            ret_buttons_sttr_list.append(name)
        return ret_buttons_sttr_list

    def spawn_bottom_buttons(self, lower_widget_bound):
        # create a label showing applied policies
        self.applied_policies_label.x = self.buttons_start_x
        self.applied_policies_label.y = self.box_y_start + lower_widget_bound - 200
        if self.applied_policies_label.text == '':
            self.applied_policies_label.text = 'Applied policies:'
        self.applied_policies_label.text_size = 14
        self.applied_policies_label.text_color = (0.2, 0.9, 0.9, 1.0)

        self.button1.y = self.box_y_start + lower_widget_bound - 160
        self.button2.y = self.box_y_start + lower_widget_bound - 160
        self.label.y = self.box_y_start + lower_widget_bound - 80
        self.iter_label.y = self.box_y_start + lower_widget_bound - 80
        self.goto_results_button.y = self.box_y_start + lower_widget_bound - 120

    def on_invoke(self, context, event):
        self.create_poilcies_dict()
        widgets_panel = [self.header, self.step_one, self.step_two,
                         self.step_three,
                         self.step_three_description, self.goto_results_button, self.models_path_label,
                         self.output_path_label,
                         self.output_path_button, self.models_path_button, self.max_features, self.avena_logo,
                         self.copyright,
                         self.label, self.button1, self.button2, self.iter_label,
                         self.www]

        widgets = [self.panel]
        widgets += widgets_panel
        self.init_widgets(context, widgets)
        self.panel.add_widgets(widgets_panel)

        # Open the panel at the mouse location
        self.panel.set_location(event.mouse_x,
                                context.area.height - event.mouse_y + 20)

    @staticmethod
    def disable_checkboxes(obj):
        attr_list = dir(obj)
        for attr in attr_list:
            if type(getattr(obj, attr)) == BL_UI_Checkbox:
                getattr(obj, attr).enabled = False

    @staticmethod
    def enable_checkboxes(obj):
        attr_list = dir(obj)
        for attr in attr_list:
            if type(getattr(obj, attr)) == BL_UI_Checkbox:
                getattr(obj, attr).enabled = True

    @staticmethod
    def disable_models_and_output_buttons(obj):
        obj.models_path_button.disabled = True
        obj.output_path_button.disabled = True

    @staticmethod
    def enable_models_and_output_buttons(obj):
        obj.models_path_button.disabled = False
        obj.output_path_button.disabled = False

    @staticmethod
    def disable_set_active_policy_buttons(obj):
        main_panel_attr_list = dir(obj)
        for attr in main_panel_attr_list:
            if 'active_policy_button_' in attr:
                current_button_state = getattr(obj, attr).disabled
                getattr(obj, attr).hidden_state = current_button_state
                getattr(obj, attr).disabled = True
        pass

    @staticmethod
    def reinstate_set_active_policy_buttons_state(obj):
        main_panel_attr_list = dir(obj)
        for attr in main_panel_attr_list:
            if 'active_policy_button_' in attr:
                hidden_state = getattr(obj, attr).hidden_state
                getattr(obj, attr).disabled = hidden_state
        pass

    # Button press handlers
    def button1_press(self, widget):
        self.save_application_state()

        DP_OT_draw_operator.disable_policy_buttons_checkboxes(self)
        DP_OT_draw_operator.disable_checkboxes(self)
        DP_OT_draw_operator.disable_models_and_output_buttons(self)
        DP_OT_draw_operator.disable_set_active_policy_buttons(self)

        # spawner.Spawner.add_table()
        #spawner.Spawner.classes_selected = dataset_creator.CreateDataset.get_class_names_selected_in_widget()
        if not dataset_creator.RUNNING:
            bpy.ops.object.createdataset('EXEC_DEFAULT')
        else:
            # resetting number of pics to spawn in next creation operation
            dataset_creator.POLICIES_LIST_OF_LISTS = list()
            for key in self.policies_dict:
                if self.policies_dict[key]['instance'] is not None and self.policies_dict[key]['instance'].policy_active:

                    pictures_limit = self.policies_dict[key]['instance'].pictures_per_policy
                    current_list = [key, self.policies_dict[key]['instance'],
                                    self.policies_dict[key]['instance'].remaining_pictures_per_policy, pictures_limit]
                    dataset_creator.POLICIES_LIST_OF_LISTS.append(current_list)

                    # update active policies for the new state of checked/unchecked boxes
                    self.policies_dict[key]['instance'].classes_dict = utils.Utils.get_classes_models_paths_dict(self,
                                                                                  self.policies_dict[key]['instance'].get_checked_classes(self))
        dataset_creator.DATASET_SIZE = self.total_samples_to_generate
        self.iter_label.text = '{} of {}'.format(str(dataset_creator.LOOP_COUNT), str(dataset_creator.DATASET_SIZE))
        bpy.ops.screen.animation_play()
        self.button1.disabled = True
        self.button2.disabled = False
        print("Button '{0}' is pressed".format(widget.text))

    def button2_press(self, widget):
        DP_OT_draw_operator.enable_policy_buttons_checkboxes(self)
        DP_OT_draw_operator.enable_checkboxes(self)
        DP_OT_draw_operator.enable_models_and_output_buttons(self)
        DP_OT_draw_operator.reinstate_set_active_policy_buttons_state(self)
        # resetting remamining pic to make in cast stop button pressed

        print("Button '{0}' is pressed".format(widget.text))
        bpy.ops.screen.animation_cancel(restore_frame=False)
        bpy.context.scene.frame_set(0)
        spawner.Spawner.delete_all_objects_in_scene()
        self.iter_label.text = '0 of {}'.format(dataset_creator.DATASET_SIZE)
        self.button1.disabled = False
        self.button2.disabled = True

    def models_path_button_press(self, widget):
        print("Button '{0}' is pressed".format(widget.text))
        DP_OT_draw_operator.disable_policy_buttons_checkboxes(self)
        DP_OT_draw_operator.disable_checkboxes(self)
        bpy.ops.test.models_dirbrowser('INVOKE_DEFAULT')
        self.models_path_button.disabled = True
        self.output_path_button.disabled = True

    def uncheck_policy_button_box_callback(self, widget):
        policy_attrs_in_main_panel = widget.policy_attached.policy_attrs_in_main_panel

        for attr in policy_attrs_in_main_panel:

            if type(getattr(self, attr)) == BL_UI_Checkbox:
                getattr(self, attr).enabled = False
                continue

            elif type(getattr(self, attr)) == BL_UI_Button:
                current_button_state = getattr(self, attr).disabled
                widget.policy_attached.hidden_set_active_button_state = current_button_state
                getattr(self, attr).disabled = True

        #set policy active poperty to calculate total samples to generate
        if widget.policy_attached.policy_active:
            widget.policy_attached.policy_active = False
            self.pics_per_policy_dict[widget.policy_attached.name]['policy_active'] = False

            # redraw samples to generate
            widget.policy_attached.get_total_samples_to_generate_from_all_policies(self)
        # below of redraws a label with active policies displayed
        active_policies = widget.policy_attached.get_active_policies()

        # if any policies are active enable the start button
        if active_policies != []:
            self.button1.disabled = False
        else:
            self.button1.disabled = True

    def check_policy_button_box_callback(self, widget):
        policy_attrs_in_main_panel = widget.policy_attached.policy_attrs_in_main_panel
        for attr in policy_attrs_in_main_panel:
            if type(getattr(self, attr)) == BL_UI_Checkbox:
                getattr(self, attr).enabled = True
                continue
            elif type(getattr(self, attr)) == BL_UI_Button:
                if widget.policy_attached.hidden_set_active_button_state == None:
                    getattr(self, attr).disabled = False
                else:
                    getattr(self, attr).disabled = widget.policy_attached.hidden_set_active_button_state

    def on_click_policy_button(self, widget):

        if widget.disabled:
            return

        if widget.text == policies_enum.PoliciesEnum.POLICY_1.name:
            if not widget.policy_attached:
                widget.policy_attached = Policies.SingleWithoutSpacing(self.arena)
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.name = policies_enum.PoliciesEnum.POLICY_1.name
                self.policies_dict[widget.policy_attached.name]['instance'] = widget.policy_attached
                widget.policy_attached.set_checkbox_attrs_names_to_add()
                widget.policy_attached.get_active_policies()

            if not widget.policy_attached.check_main_widget_has_policy_attrs(self):
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self)

            else:

                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self, attrs_present=True)


        elif widget.text == policies_enum.PoliciesEnum.POLICY_2.name:

            if not widget.policy_attached:
                widget.policy_attached = Policies.ContainerWithItems(self.arena)
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.name = policies_enum.PoliciesEnum.POLICY_2.name
                self.policies_dict[widget.policy_attached.name]['instance'] = widget.policy_attached
                widget.policy_attached.set_checkbox_attrs_names_to_add()
                widget.policy_attached.get_active_policies()

            if not widget.policy_attached.check_main_widget_has_policy_attrs(self):
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self)


            else:

                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self, attrs_present=True)


        elif widget.text == policies_enum.PoliciesEnum.POLICY_3.name:

            if not widget.policy_attached:
                widget.policy_attached = Policies.SingleWithSpacing(self.arena)
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.name = policies_enum.PoliciesEnum.POLICY_3.name
                self.policies_dict[widget.policy_attached.name]['instance'] = widget.policy_attached
                widget.policy_attached.set_checkbox_attrs_names_to_add()
                widget.policy_attached.get_active_policies()

            if not widget.policy_attached.check_main_widget_has_policy_attrs(self):
                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self)

            else:

                widget.policy_attached.policy_button_y = widget.y
                widget.policy_attached.draw_policy(self, attrs_present=True)

        elif widget.text == policies_enum.PoliciesEnum.POLICY_4.name:
            print("Policy 4")

        elif widget.text == policies_enum.PoliciesEnum.POLICY_5.name:
            print("Policy 5")

        elif widget.text == policies_enum.PoliciesEnum.POLICY_6.name:
            print("Policy 6")

        elif widget.text == policies_enum.PoliciesEnum.POLICY_7.name:
            print("Policy 7")

        else:
            print("Button name does not match Policy name in policies_enum")

    def check_policy_buttons_exist(self):
        policies_attr_list = [name for name, member in policies_enum.PoliciesEnum.__members__.items()]
        for attr in dir(self):
            if attr in policies_attr_list:
                return True
        return False

    def output_path_button_press(self, widget):
        print("Button '{0}' is pressed".format(widget.text))
        DP_OT_draw_operator.disable_policy_buttons_checkboxes(self)
        DP_OT_draw_operator.disable_checkboxes(self)
        bpy.ops.test.output_dirbrowser('INVOKE_DEFAULT')
        self.models_path_button.disabled = True
        self.output_path_button.disabled = True
        # self.spawn_checkboxes()
        # check if policy buttons already exist, if yes, just return
        if not self.check_policy_buttons_exist():
            self.spawn_policy_buttons()

        else:
            return
        # self.disable_checkboxes()

    def goto_results_button_press(self, widget):
        print("Button '{0}' is pressed".format(widget.text))
        bashCommand = "xdg-open " + self.output_dir_path
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()

    def on_www_clicked(self, widget):
        print("Button '{0}' is pressed".format(widget.text))
        bashCommand = "xdg-open " + "http://" + self.www.text
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
