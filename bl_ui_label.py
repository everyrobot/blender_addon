from .bl_ui_widget import *

import blf


class BL_UI_Label(BL_UI_Widget):

    def __init__(self, x, y, width, height):
        super().__init__(x, y, width, height)

        self._text_color = (1.0, 1.0, 1.0, 1.0)
        self._text = "Label"
        self._text_size = 16
        self._visible = True


    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, value):
        self._visible = value

    # @property
    # def draw_counter(self):
    #     return self._draw_counter
    #
    # @draw_counter.setter
    # def draw_counter(self, value):
    #     self._draw_counter = value
    #
    # @property
    # def alt_text_color(self):
    #     return self._alt_text_color
    #
    # @alt_text_color.setter
    # def alt_text_color(self, value):
    #     self._alt_text_color = value

    @property
    def text_color(self):
        return self._text_color

    @text_color.setter
    def text_color(self, value):
        self._text_color = value

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @property
    def text_size(self):
        return self._text_size

    @text_size.setter
    def text_size(self, value):
        self._text_size = value

    def is_in_rect(self, x, y):
        return False


    def draw(self):
        # print(self._text, " visible: ", self._visible )
        if self._visible:
            area_height = self.get_area_height()

            blf.size(0, self._text_size, 72)
            size = blf.dimensions(0, self._text)

            textpos_y = area_height - self.y_screen - self.height
            blf.position(0, self.x_screen, textpos_y, 0)

            # if self._alt_text_color and not self._color_changed:
            #     self.draw_counter += 1
            #     if self._draw_counter > 3:
            #         print("DRAW COUNTER: ", self._draw_counter)
            #         r, g, b, a = self._alt_text_color
            #         self._color_changed = True
            #         self._draw_counter = 0
            #         print("FIRST IF")
            #     else:
            #         r, g, b, a = self._text_color
            #
            # elif self._alt_text_color and self._color_changed:
            #     self.draw_counter += 1
            #     if self._draw_counter > 3:
            #         r, g, b, a = self._text_color
            #         self._color_changed = False
            #         self._draw_counter = 0
            #         print("SECOND IF")
            #     else:
            #         r, g, b, a = self._alt_text_color
            # else:
            #     r, g, b, a = self._text_color
            #     print("ELSE IF")

            r, g, b, a = self._text_color
            blf.color(0, r, g, b, a)
            blf.draw(0, self._text)
