import glob
import json
import math
import os
import random
from .robot_files.avena_robot import avena_robot
from .robot_files.franka_robot import franka_robot
SPAWNED_OBJ_POSTFIX = 'spawned'
WITH_CONE_POSTFIX = 'cone'

CONTAINERS_ROOT = ''
CONSUMABLES_ROOT = ''
TOOLS_ROOT = ''
CONTAINERS_ROOT_DICT = ("containers", '')
CONSUMABLES_ROOT_DICT = ("consumables", '')
TOOLS_ROOT_DICT = ("tools", '')

mesh_collisions = ['WoodenBowl', 'SmallPlate', 'SmallMattePlate', 'SmallBowl', 'PlainPlasticBoard', 'LargePlate',
                   'DoubleSideBowl', 'BorderPlasticBoard', 'BigWoodenBoard', 'MediumPlate']
OUTPUT_DIR = ''
CONFIG_FILE = os.path.join('/home', os.environ.get('USER'), '.config', 'blender', '2.90', 'scripts',
                           'addons', 'blender_addon', 'segmentation_config.json')
REGION_FILE = os.path.join('/home', os.environ.get('USER'), '.config', 'blender', '2.90', 'scripts',
                           'addons', 'blender_addon', 'region.json')
SPAWN_IN_CONTAINERS_POLICIES = ["POLICY_2"]

# DIRECTORY FOR HDRI FILES
HDRI_DIR = os.path.join('/home', os.environ.get('USER'), 'BLENDER_RESOURCES', 'hdri_all')

# FLAGS FOR SPAWNING OBJECTS WITH A WEIGHTED PROBABILITY
WEIGHTED_SPAWNING = False
WEIGHTED_SPAWNING_THRESHOLD = 0.6
WEIGHTED_CLASS_CHOICE = ['SmallPlate']

# CATEGORIES SECTION
CONTAINERS_CATEGORY_INDEX = 1
CAT_DIR_NAMES = ['Consumables', 'Containers', 'Tools']
CAT_LIST = []

# HANDLE PREFIXES
SMALL_HANDLE_NAME = 'ShortHandle'
LONG_HANDLE_NAME = 'LongHandle'
SPLIT_OBJ_HEAD = 'Head'
SPLIT_OBJ_HANDLE = 'Handle'

# SIZE RANDOMIZATION OBJECT NAMES AND SUB-STRINGS
DONT_RANDOMIZE_POS = 'dontrandomize'
DONT_RANDOMIZE_SIZE = 'nosizechange'
CLASSES_TO_RAND_SIZE_DOWN = ['Peppers', 'Onions', 'Broccoli']
CLASSES_TO_RAND_SIZE_UP = ["Apple", 'Lemon', 'Orange', 'Cucumber', 'Carrot', 'Tomato', 'Banana']

# SPECIAL PROPERTIES OBJECT NAMES AND SUB-STRINGS
CLASSES_WITH_CONE = ['WoodenBowl', 'SmallPlate', 'SmallMattePlate', 'SmallBowl', 'PlainPlasticBoard', 'LargePlate',
                     'DoubleSideBowl', 'BorderPlasticBoard', 'BigWoodenBoard', 'MediumPlate']
CLASSES_WITH_CONE_POLICY_3 = ['WoodenBowl', 'SmallPlate', 'SmallMattePlate', 'SmallBowl', 'PlainPlasticBoard',
                              'LargePlate', 'DoubleSideBowl', 'BorderPlasticBoard', 'BigWoodenBoard', 'MediumPlate']
OBJECTS_WITH_PARTS = ['Broccoli', 'Onion', 'Knife', 'Ladle', 'CurvedSpatula', 'SpatulaMintGrey', 'SpatulaPinkBlack',
                      'PlainPlasticBoard', 'BorderPlasticBoard']
OBJECTS_WITH_SMALL_HANDLES = ['SpatulaMintGrey', 'SpatulaPinkBlack']
OBJECTS_WITH_LONG_HANDLES = ['Knife', 'Ladle', 'CurvedSpatula', 'PlainPlasticBoard', 'BorderPlasticBoard']
SMALL_HANDLE_OBJECTS = {'SpatulaPinkBlack', 'SpatulaMintGrey'}
LONG_HANDLE_OBJECTS = {'Knife', 'Ladle', 'CurvedSpatula', 'PlainPlasticBoard', 'BorderPlasticBoard'}

# SECTION FOR ROBOT PARTS
FIRST_ROBOT_SPAWN = True
ROBOT = None
FIRST_FRANKA_SPAWN = True
FRANKA_ROBOT = None
FRANKA_ROBOT_NAME_SCENE = 'Link11'
AVENA_ROBOT_NAME_SCENE = 'Link 19'
SPAWN_ROBOT = False
RENDER_MASKS = False
GENERATE_DEPTH = False

class Area:
    def __init__(self, origin, diagonal):
        self.origin = origin
        self.diagonal = diagonal

    @staticmethod
    def get_arena_from_file():
        with open(REGION_FILE) as f:
            region_data = json.load(f)
            origin = Point(
                region_data["origin"]["x"],
                region_data["origin"]["y"],
                region_data["origin"]["z"]
            )
            diagonal = Point(
                region_data["diagonal"]["x"],
                region_data["diagonal"]["y"],
                region_data["diagonal"]["z"]
            )
            return Area(origin, diagonal)


class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        pass


class Utils:
    @staticmethod
    def point_distance(point1, point2):
        a = abs(point1[0] - point2[0])
        b = abs(point2[1] - point2[1])
        return math.sqrt(a * a + b * b)

    @staticmethod
    def rand_float(start, stop):
        return random.uniform(start, stop)

    @staticmethod
    def random_int(start, stop):
        return random.randint(start, stop)

    @staticmethod
    def list_cat_dirs(categories_root):
        return glob.glob(categories_root + '/*/')

    @staticmethod
    def list_class_dirs(models_root_dir):
        return glob.glob(models_root_dir + '/*/')

    @staticmethod
    def get_classes_models_paths_dict(main_panel, classes_names_list, item_type=None):
        assert (
                item_type is None or
                item_type == "Containers" or
                item_type == "Consumables" or
                item_type == "Tools"
        ), "Wrong item type"
        print("CLASSES_NAMES_LIST: ", classes_names_list)
        result = dict()
        models_classes_paths = []
        for cls_tuple in CAT_LIST:
            models_classes_paths += Utils.list_class_dirs(cls_tuple[1])

        chose_model_classes_paths = []
        for class_path in models_classes_paths:
            if any(class_name in class_path for class_name in classes_names_list):
                chose_model_classes_paths.append(class_path)
        classes_names_list.sort()
        chose_model_classes_paths.sort(key=lambda x: x.split('/')[-2])

        for class_name, chose_model_path in zip(classes_names_list, chose_model_classes_paths):
            if item_type is None or str(item_type) in chose_model_path:
                result[class_name] = Utils.list_models_in_class_dir(chose_model_path)
        return result

    @staticmethod
    def list_models_in_class_dir(class_dir):
        instance_dir_list = glob.glob(os.path.join(class_dir, "*"))
        obj_model_list = list()
        for instance_dir in instance_dir_list:
            obj_files_list = glob.glob(os.path.join(instance_dir, "*.obj"))
            obj_model_list.append(obj_files_list[0])
        return obj_model_list
