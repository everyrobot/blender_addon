import cv2
import numpy as np

img = cv2.imread('/home/oem/dataset/0_sample.png')
pts = np.array([[300, 180], [1660, 180], [1950, 920], [20, 920]])


stencil = np.zeros(img.shape).astype(img.dtype)
contours = [pts]
color = [255, 255, 255]
cv2.fillPoly(stencil, contours, color)
result = cv2.bitwise_and(img, stencil)
cv2.imshow("output", result)
cv2.waitKey(0)