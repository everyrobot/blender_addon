import enum
from .Policies import SingleWithoutSpacing
from .Policies import ContainerWithItems
from .Policies import SingleWithSpacing


class PoliciesEnum(enum.Enum):

    POLICY_1 = SingleWithoutSpacing
    POLICY_2 = ContainerWithItems
    POLICY_3 = SingleWithSpacing
    POLICY_4 = 4
    POLICY_5 = 5
    POLICY_6 = 6
    POLICY_7 = 7
