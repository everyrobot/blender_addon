import bpy
import os
from . import utils
import time
from . import object_manipulator
from . import physics_mainpulator


class Spawner:
    def __init__(self, scene_origin, scene_diagonal):
        self.object_manipulator = object_manipulator.ObjectManipulator()
        self.physics_mainpulator = physics_mainpulator.PhysicsManipulator()
        self.scene = bpy.context.scene
        self.scene_origin = scene_origin
        self.scene_diagonal = scene_diagonal
        self.utls = utils.Utils()

    classes_selected = dict()

    def choose_n_random_spots(self, n, radius, area):
        results_list = []
        max_error_amount = 100
        current_error_amount = 0
        while len(results_list) < n and current_error_amount < max_error_amount:
            x = utils.Utils.rand_float(area.origin.x, area.diagonal.x)
            y = utils.Utils.rand_float(area.origin.y, area.diagonal.y)
            append = True
            for point in results_list:
                if utils.Utils.point_distance((x, y), point) < radius:
                    append = False
                    current_error_amount += 1
                    break
            if append:
                current_error_amount = 0
                results_list.append((x, y))
        return results_list

    def spawn_containers_with_position_from_list(self, paths_list, spots):
        if len(spots) < len(paths_list):
            paths_list = paths_list[:len(spots)]

        for item in paths_list:
            path_parts = item.split(os.path.sep)
            print(path_parts[-3])
            self.spawn_object(item, path_parts[-3])

        containers_list = [item.name
                           for item in bpy.data.objects
                           if item.type == "MESH" and
                           utils.SPAWNED_OBJ_POSTFIX in item.name and
                           utils.WITH_CONE_POSTFIX in item.name]

        for n, container in enumerate(containers_list):
            bpy.data.objects[container].location.x = spots[n][0]
            bpy.data.objects[container].location.y = spots[n][1]

    def spawn_items_from_list_with_random_position(self, paths_list, set_containers_physics=True):
        for item in paths_list:
            path_parts = item.split(os.path.sep)
            print('PATH PARTS', path_parts)
            print('CLASSES WITH CONE ', utils.CLASSES_WITH_CONE)
            if any(label in path_parts[-3] for label in utils.CLASSES_WITH_CONE):
                self.spawn_object(item, path_parts[-3], with_cone=True)
            else:
                self.spawn_object(item, path_parts[-3])
        self.object_manipulator.set_all_object_origin_to_geometry()
        self.object_manipulator.randomize_all_objects_position_in_area(self.scene_origin, self.scene_diagonal)
        self.object_manipulator.randomize_all_object_rotation()
        self.object_manipulator.randomize_aselected_object_size()
        self.physics_mainpulator.set_all_object_rigid_body_and_collision(set_containers_physics)
        self.physics_mainpulator.set_friction_for_all_objects(1.0)
        self.physics_mainpulator.set_angular_damping(0.5)

    def random_spawn_items(self, origin, diagonal):
        # origin = Point(-1.5, -1.5, 0)
        # diagonal = Point(1.5, 1.5, 1)
        print("RANDOM SPAWN ITEMS")
        self.spawn_n_random_objects(10)
        # self.spawn_bowl_with_random_items([0, 0, -0.38], 4)
        self.object_manipulator.set_all_object_origin_to_geometry()
        self.object_manipulator.randomize_all_objects_position_in_area(origin, diagonal)

        self.object_manipulator.randomize_all_object_rotation()

        self.physics_mainpulator.set_all_object_rigid_body_and_collision()
        self.physics_mainpulator.set_friction_for_all_objects(1.0)
        self.physics_mainpulator.set_angular_damping(0.5)

    def spawn_n_random_items_in_location(self, no_items, location):
        self.spawn_n_random_objects(no_items, location, False)

    def spawn_n_random_objects(self, no_of_objects, location=None, randomize_pos=True):
        if location is None:
            location = []
        class_dirs = list(Spawner.classes_selected.values())
        for i in range(no_of_objects):
            class_dir = class_dirs[utils.Utils.random_int(0, len(class_dirs) - 1)]
            print("CLASS DIR: ", class_dir)
            print("CLASS DIR: ", class_dir)
            class_name = class_dir.split('/')[-2:][0] + '_' + str(time.time())
            models_available = utils.Utils.list_models_in_class_dir(class_dir)
            print("MODELS: ", models_available)
            # if spawning in a specific location
            # TODO: fix here spawning above the bowl && check if randomize_pos actaully works by rpintouts
            if location:
                val = location[2]
                location[2] = val + 0.3
            self.spawn_random_instance_from_class(models_available, 1, class_name, location, randomize_pos)

    def spawn_random_instance_from_class(self, models_list, no_of_spawns, class_name, location=None, randomize_pos=True,
                                         max_friction=True):
        if location is None:
            location = []
        for i in range(no_of_spawns):
            self.spawn_object(models_list[utils.Utils.random_int(0, len(models_list) - 1)], class_name, location,
                              randomize_pos)

    def spawn_object(self, path, class_name, location=None, randomize_pos=True, with_cone=False):
        print(path)
        print(class_name)
        if location is None:
            location = []
        bpy.ops.import_scene.obj(filepath=path)
        self.physics_mainpulator.set_rigid_body_world_settings()

        if randomize_pos:
            name = class_name + '_' + utils.SPAWNED_OBJ_POSTFIX
        else:
            name = class_name + '_' + utils.SPAWNED_OBJ_POSTFIX + '_' + utils.DONT_RANDOMIZE_POS
        print('WITH CONE: ', str(with_cone))
        if with_cone:
            name += "_" + utils.WITH_CONE_POSTFIX
        bpy.context.selected_objects[0].name = name
        bpy.context.selected_objects[0].data.name = name

        if location:
            bpy.context.selected_objects[0].location = location

    @staticmethod
    def add_table():
        bpy.ops.mesh.primitive_cube_add()
        scene = bpy.context.scene
        for obj in scene.objects:
            # TODO: change hardcoded shpae name to something descriptive, otherwise affects all 'Cube's
            if obj.name == 'Cube':
                bpy.ops.object.select_all(action='DESELECT')
                obj.select_set(True)
                bpy.context.view_layer.objects.active = obj
                obj.dimensions = [12, 12, 0.2]
                obj.location = [0, 0, -0.5]
                obj.scale = [1.5, 1.5, 0.1]
                bpy.ops.rigidbody.object_add(type='PASSIVE')

    @staticmethod
    def delete_all_objects_in_scene(only_fruit=True):
        obj_mainpulator = object_manipulator.ObjectManipulator()
        bpy.ops.object.select_all(action='DESELECT')

        if only_fruit:
            candidate_list = [item.name for item in bpy.data.objects if utils.SPAWNED_OBJ_POSTFIX in item.name]
            robot_list = [item.name for item in bpy.data.objects if 'Arm' in item.name]
            candidate_list += robot_list
        else:
            candidate_list = [item.name for item in bpy.data.objects]

        for obj in candidate_list:
            if 'Camera' in obj:
                continue
            # select the object
            bpy.data.objects[obj].select_set(True)
            # delete all selected objects
            # bpy.data.objects.remove(bpy.data.objects[obj], do_unlink=True)
            bpy.ops.object.delete(use_global=True)
        obj_mainpulator.delete_meshes()