import bpy

from . import drag_panel_op
from . import object_manipulator
from . import physics_mainpulator
from . import segmentation_manager
from . import spawner
from . import utils
from .robot_files.avena_robot import avena_robot
from .robot_files.franka_robot import franka_robot
import glob
import random
import math


RUNNING = False
LOOP_COUNT = 0
DATASET_SIZE = 0
CLASSES_TO_SPAWN = None
WIDGET_SPAWNER = None
CURRENT_POLICY_GENERATED = 0
POLICIES_LIST_OF_LISTS = list()
RENDER_FLAG = True
FIRST_HDRI = True
HDRI_DIR = utils.HDRI_DIR


class CreateDataset(bpy.types.Operator):
    """My Object Moving Script"""  # Use this as a tooltip for menu items and buttons.
    bl_idname = "object.createdataset"  # Unique identifier for buttons and menu items to reference.
    bl_label = "Create a dataset"  # Display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}  # Enable undo for the operator.

    def __init__(self):
        global WIDGET_SPAWNER
        self.origin = utils.Area.get_arena_from_file().origin
        self.diagonal = utils.Area.get_arena_from_file().diagonal
        self.arena = utils.Area(origin=self.origin, diagonal=self.diagonal)
        self.spwn = spawner.Spawner(self.origin, self.diagonal)
        self.policies_list_of_lists = list()
        WIDGET_SPAWNER = self.spwn
        self.first_hdri = True


    @staticmethod
    def get_class_names_selected_in_widget():
        ret_dict = dict()
        widget = drag_panel_op.WIDGET_INSTANCE
        attr_list = dir(widget)
        print("WIDGET ATTRS: ", attr_list)
        for k, v in widget.classes_to_check_for_check.items():
            if getattr(widget, k).is_checked:
                ret_dict[k] = v
        return ret_dict

    @staticmethod
    def set_random_hdri(hdri_dir_path):
        global FIRST_HDRI
        hrd_list = glob.glob(hdri_dir_path + '*.hdr')
        print('HDRI PATH: ', hdri_dir_path)
        exr_list = glob.glob(hdri_dir_path + '*.exr')
        hdri_list = hrd_list + exr_list
        chosen_hdri = random.choice(hdri_list)
        print("HDRI chosen: ", chosen_hdri)
        context = bpy.context
        world = context.scene.world
        world.use_nodes = True
        node_tree = bpy.data.worlds[world.name].node_tree
        random_light_strength = random.uniform(-3.0, 1.0)

        if FIRST_HDRI:

            enode = context.scene.world.node_tree.nodes.new("ShaderNodeTexEnvironment")
            enode.image = bpy.data.images.load(chosen_hdri)
            enode.location.x = 200
            enode.location.y = 100
            tex_color_out = enode.outputs["Color"]
            world_output = world.node_tree.nodes["World Output"]
            # background_node = world.node_tree.nodes["Background"]
            surface_color_in = world_output.inputs["Surface"]
            node_tree.links.new(tex_color_out, surface_color_in)
            coord_in = enode.inputs["Vector"]

            texture_coord_node = context.scene.world.node_tree.nodes.new("ShaderNodeTexCoord")
            coord_out = texture_coord_node.outputs["Generated"]

            mapping_node = context.scene.world.node_tree.nodes.new("ShaderNodeMapping")
            mapping_in = mapping_node.inputs["Vector"]
            random_deg = random.uniform(0, 360)
            node_tree.nodes['Mapping'].inputs[2].default_value[2] = math.radians(random_deg)
            node_tree.links.new(coord_out, mapping_in)

            mapping_out = mapping_node.outputs["Vector"]
            node_tree.links.new(mapping_out, coord_in)

            bpy.data.scenes["Scene"].view_settings.exposure = random_light_strength

        else:
            node_tree.nodes['Environment Texture'].image = bpy.data.images.load(chosen_hdri)
            random_deg = random.uniform(0, 360)
            node_tree.nodes['Mapping'].inputs[2].default_value[2] = math.radians(random_deg)
            bpy.data.scenes["Scene"].view_settings.exposure = random_light_strength

    def set_selected_boxes_in_spawner(self):
        spawner.Spawner.classes_selected = CreateDataset.get_class_names_selected_in_widget()

    def execute(self, context):  # execute() is called when running the operator.
        bpy.data.scenes["Scene"].render.engine = "CYCLES"
        bpy.data.scenes["Scene"].cycles.device = "GPU"
        bpy.data.scenes["Scene"].frame_end = 90

        global RUNNING
        global POLICIES_LIST_OF_LISTS
        RUNNING = True
        widget = drag_panel_op.WIDGET_INSTANCE
        origin = self.arena.origin
        diagonal = self.arena.diagonal
        # self.spwn = spawner.Spawner(origin, diagonal)
        manipulator = object_manipulator.ObjectManipulator()
        # spawner.Spawner.classes_selected = CreateDataset.get_class_names_selected_in_widget()
        segmentator = segmentation_manager.SegmentationManager()
        print("DATASET DESTINATION PATH", segmentator.dataset_destination_path)

        # get all policies and samples to generate per policy
        policies_dict = widget.policies_dict
        # for each policy get number of samples to generate
        print("policies_dict: ", policies_dict)
        for key in policies_dict:
            if policies_dict[key]['instance'] is not None:
                pictures_limit = policies_dict[key]['instance'].pictures_per_policy
                current_list = [key, policies_dict[key]['instance'],
                                policies_dict[key]['instance'].remaining_pictures_per_policy, pictures_limit]
                self.policies_list_of_lists.append(current_list)
        POLICIES_LIST_OF_LISTS = self.policies_list_of_lists

        physic_manipulator = physics_mainpulator.PhysicsManipulator()
        # spwn.classes_selected = checked_paths
        segmentator.create_compositing_node_tree()
        # segmentator = self.segmentator

        spawner.Spawner.classes_selected = CreateDataset.get_class_names_selected_in_widget()

        def pre_frame(scene):
            global LOOP_COUNT
            global CURRENT_POLICY_GENERATED
            global POLICIES_LIST_OF_LISTS
            global RENDER_FLAG
            global FIRST_HDRI
            global HDRI_DIR
            widget = drag_panel_op.WIDGET_INSTANCE
            if CURRENT_POLICY_GENERATED < len(POLICIES_LIST_OF_LISTS):
                current_policy_instance = POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][1]

            if scene.frame_current == 2:
                # if there is a policy to use
                print("CURRENT_POLICY_GENERATED: ", CURRENT_POLICY_GENERATED)
                print("len(policies_list_of_lists): ", len(POLICIES_LIST_OF_LISTS))
                if CURRENT_POLICY_GENERATED < len(POLICIES_LIST_OF_LISTS):
                    # if there are still pictures to make
                    print("policies_list_of_lists[CURRENT_POLICY_GENERATED][2]: ",
                          POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][2])
                    print("POLICIES_LIST_OF_LISTS: ", POLICIES_LIST_OF_LISTS)
                    if POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED] is not None and \
                            POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][2] > 0:

                        # apply policy
                        if utils.SPAWN_ROBOT:
                            avr = avena_robot.Robot(cam_name='cam_1')
                            frr = franka_robot.Robot(cam_name='cam_1')
                            avr.shift_robot_pose()
                            frr.shift_robot_pose()


                        current_policy_instance.apply()
                        # decrement number of pictures to take in the policy tuple
                        POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][2] -= 1

                    # if no more pictures to generate in this policy go to next
                    else:
                        # move to the next policy
                        CURRENT_POLICY_GENERATED += 1
                        print('CURRENT_POLICY_GENERATED: ', CURRENT_POLICY_GENERATED)
                        if CURRENT_POLICY_GENERATED < len(POLICIES_LIST_OF_LISTS):
                            current_policy_instance = POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][1]
                else:
                    # set ui state to initial state
                    CURRENT_POLICY_GENERATED = 0
                    bpy.ops.screen.animation_cancel(restore_frame=False)
                    bpy.context.scene.frame_set(0)
                    LOOP_COUNT = 0
                    spawner.Spawner.delete_all_objects_in_scene()
                    drag_panel_op.WIDGET_INSTANCE.load_saved_application_state()
                    widget.button1.disabled = False
                    widget.button2.disabled = True

            if scene.frame_current == 40:
                current_policy_instance.apply_post_physics()
                current_policy_instance.apply_post_items()
                pass


            if scene.frame_current == 60:
                manipulator.delete_cone_vertex_group_from_containers()

                if FIRST_HDRI:
                    CreateDataset.set_random_hdri(HDRI_DIR)
                    FIRST_HDRI = False
                else:
                    CreateDataset.set_random_hdri(HDRI_DIR)

            if scene.frame_current == 84:
                manipulator.stop_object_movement()


            if scene.frame_current == 85:
                segmentator.set_index_ob_pass()

            if scene.frame_current == 86:
                if RENDER_FLAG:
                    RENDER_FLAG = False

                    segmentator.render_sample()

            if scene.frame_current == 90:
                RENDER_FLAG = True
                if POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][2] <= 0:

                    # move to the next policy
                    CURRENT_POLICY_GENERATED += 1
                    print('CURRENT_POLICY_GENERATED: ', CURRENT_POLICY_GENERATED)
                    if CURRENT_POLICY_GENERATED < len(POLICIES_LIST_OF_LISTS):
                        current_policy_instance = POLICIES_LIST_OF_LISTS[CURRENT_POLICY_GENERATED][1]

                spawner.Spawner.delete_all_objects_in_scene()
                LOOP_COUNT += 1
                widget.iter_label.text = '{} of {}'.format(str(LOOP_COUNT), str(widget.total_samples_to_generate))

        # add one of these functions to frame_change_pre handler:
        bpy.app.handlers.frame_change_pre.append(pre_frame)

        # if not bpy.context.screen.is_animation_playing:
        bpy.ops.screen.animation_play()
        bpy.ops.screen.animation_cancel(True)

        return {'FINISHED'}  # Lets Blender know the operator finished successfully.
