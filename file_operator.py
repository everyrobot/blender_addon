from bpy_extras.io_utils import ImportHelper
from bpy.types import Operator
from . import drag_panel_op
from . import utils
from .bl_ui_checkbox import BL_UI_Checkbox
from .drag_panel_op import DP_OT_draw_operator
import bpy


class ConfigFileOperator(Operator, ImportHelper):
    bl_idname = "test.config_filebrowser"
    bl_label = "Select"

    filepath: bpy.props.StringProperty(
        name="Outdir Path",
        description="Where I will save my stuff"
    )

    def execute(self, context):
        widget = drag_panel_op.WIDGET_INSTANCE
        drag_panel_op.WIDGET_INSTANCE.configfile_path_label.text = self.filepath[:77] + '...' if len(
            self.filepath) > 80 else self.filepath
        drag_panel_op.WIDGET_INSTANCE.config_file_path = self.filepath
        print("UI PATH: ", drag_panel_op.WIDGET_INSTANCE.configfile_path_label.text)
        # widget.button1.disabled = False
        attr_list = dir(widget)
        for attr in attr_list:
            if type(getattr(widget, attr)) == BL_UI_Checkbox:
                getattr(widget, attr).enabled = True
        widget.step_five.visible = True

        return {'FINISHED'}


class ModelsDirOperator(Operator, ImportHelper):
    bl_idname = "test.models_dirbrowser"
    bl_label = "Select"

    directory: bpy.props.StringProperty(
        name="Outdir Path",
        description="Where I will save my stuff"
    )

    def execute(self, context):
        widget = drag_panel_op.WIDGET_INSTANCE
        widget.models_path_label.text = self.directory[:72] + '...' if len(self.directory) > 75 else self.directory
        widget.models_dir_path = self.directory

        cat_names = utils.CAT_DIR_NAMES
        go_ahead = False
        for path in utils.Utils.list_cat_dirs(self.directory):
            if any(cat_name in path for cat_name in cat_names):
                go_ahead = True
            else:
                go_ahead = False

        if go_ahead:
            utils.CONSUMABLES_ROOT = self.directory + 'Consumables'
            utils.CONTAINERS_ROOT = self.directory + 'Containers'
            utils.TOOLS_ROOT = self.directory + 'Tools'

            utils.CONSUMABLES_ROOT_DICT = ('consumables', self.directory + 'Consumables')
            utils.CONTAINERS_ROOT_DICT = ('containers', self.directory + 'Containers')
            utils.TOOLS_ROOT_DICT = ('tools', self.directory + 'Tools')

            if utils.CONSUMABLES_ROOT_DICT not in utils.CAT_LIST:
                utils.CAT_LIST.append(utils.CONSUMABLES_ROOT_DICT)

            if utils.CONTAINERS_ROOT_DICT not in utils.CAT_LIST:
                utils.CAT_LIST.append(utils.CONTAINERS_ROOT_DICT)

            if utils.TOOLS_ROOT_DICT not in utils.CAT_LIST:
                utils.CAT_LIST.append(utils.TOOLS_ROOT_DICT)

            print("UI PATH: ", widget.models_path_label.text)
            widget.models_path_button.disabled = False
            widget.output_path_button.disabled = False
            DP_OT_draw_operator.enable_policy_buttons_checkboxes(widget)
            DP_OT_draw_operator.enable_checkboxes(widget)
            widget.step_two.visible = True
        else:
            #TODO: add erro printout informin of a wrong models path
            print("UI PATH: ", widget.models_path_label.text)
            widget.models_path_button.disabled = False
            widget.output_path_button.disabled = True
            DP_OT_draw_operator.enable_policy_buttons_checkboxes(widget)
            DP_OT_draw_operator.enable_checkboxes(widget)
            widget.step_two.visible = False
        return {'FINISHED'}

    def cancel(self, context):
        widget = drag_panel_op.WIDGET_INSTANCE
        widget.models_path_button.disabled = False
        if widget.step_two.visible:
            widget.output_path_button.disabled = False
        DP_OT_draw_operator.enable_policy_buttons_checkboxes(widget)
        DP_OT_draw_operator.enable_checkboxes(widget)


class OutputDirOperator(Operator, ImportHelper):
    bl_idname = "test.output_dirbrowser"
    bl_label = "Select"

    directory: bpy.props.StringProperty(
        name="Outdir Path",
        description="Where I will save my stuff"
    )

    def execute(self, context):
        widget = drag_panel_op.WIDGET_INSTANCE
        drag_panel_op.WIDGET_INSTANCE.output_path_label.text = self.directory[:72] + '...' if len(
            self.directory) > 75 else self.directory
        drag_panel_op.WIDGET_INSTANCE.output_dir_path = self.directory
        print("UI PATH: ", drag_panel_op.WIDGET_INSTANCE.output_path_label.text)
        widget.goto_results_button.disabled = False
        widget.step_three.visible = True
        widget.step_three_description.visible = True
        widget.models_path_button.disabled = False
        widget.output_path_button.disabled = False
        utils.OUTPUT_DIR = self.directory
        DP_OT_draw_operator.enable_policy_buttons_checkboxes(widget)
        DP_OT_draw_operator.enable_checkboxes(widget)

        # attr_list = dir(widget)
        # for attr in attr_list:
        #     if type(getattr(widget, attr)) == BL_UI_Checkbox:
        #         getattr(widget, attr).enabled = True

        return {'FINISHED'}

    def cancel(self, context):
        widget = drag_panel_op.WIDGET_INSTANCE
        widget.models_path_button.disabled = False
        widget.output_path_button.disabled = False
        DP_OT_draw_operator.enable_policy_buttons_checkboxes(widget)

        if widget.output_path_label.text != 'None':
            DP_OT_draw_operator.enable_checkboxes(widget)
