import bpy
import time
import json
from . import utils


class SegmentationManager:
    def __init__(self):
        self.config_file_path = utils.CONFIG_FILE
        self.dataset_destination_path = utils.OUTPUT_DIR
        with open(self.config_file_path) as f:
            self.segmentation_config = json.load(f)
        assert self.segmentation_config is not None, "segmentation_config returned null value"

        self.current_idx = {}
        for key in self.segmentation_config:
            self.current_idx[key] = self.segmentation_config[key]["min"]

        print(self.current_idx)

    def set_index_ob_pass(self):
        candidate_list = [item.name for item in bpy.data.objects if utils.SPAWNED_OBJ_POSTFIX in item.name]

        if utils.SPAWN_ROBOT:
            candidate_list.append('AvenaArm')
            candidate_list.append('FrankaArm')


        assert len(candidate_list) > 0, "No object detected on the scene"
        scene = bpy.context.scene
        for item in candidate_list:
            print(item)
            for prefix in self.segmentation_config:
                if item.startswith(prefix):
                    # if f.ex broccoli prefix is in a list of objects to be split
                    if prefix in utils.OBJECTS_WITH_PARTS:
                        for obj in bpy.data.objects:
                            obj.select_set(False)
                        obj = scene.objects[item]
                        obj_location = obj.matrix_world.to_translation()
                        obj_rotation = obj.matrix_world.to_euler()
                        bpy.context.view_layer.objects.active = obj
                        bpy.ops.rigidbody.object_add(type='PASSIVE')
                        obj.location = obj_location
                        obj.rotation_euler = obj_rotation
                        bpy.ops.object.mode_set(mode='EDIT')
                        bpy.ops.mesh.separate(type='LOOSE')  # Separate by material
                        # After separating, all pieces are selected by default
                        obj_list = [bpy.context.selected_objects[0], bpy.context.view_layer.objects.active]
                        for o in obj_list:
                            if o.name == item: # if the selected object has same name as item variable then it is the 'head' of the broccoli
                                # if belongs to small handle category
                                if prefix in utils.OBJECTS_WITH_SMALL_HANDLES:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[prefix + utils.SPLIT_OBJ_HEAD]
                                    for head_name in utils.SMALL_HANDLE_OBJECTS:
                                        self.current_idx[head_name + utils.SPLIT_OBJ_HEAD] += 1
                                        print('current color id: ', self.current_idx[head_name + utils.SPLIT_OBJ_HEAD],
                                              'for class: ', head_name + utils.SPLIT_OBJ_HEAD)
                                        assert self.current_idx[head_name + utils.SPLIT_OBJ_HEAD] <= \
                                               self.segmentation_config[head_name + utils.SPLIT_OBJ_HEAD][
                                                   "max"], "ID pass out of range"

                                elif prefix in utils.OBJECTS_WITH_LONG_HANDLES:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[prefix + utils.SPLIT_OBJ_HEAD]
                                    for head_name in utils.LONG_HANDLE_OBJECTS:
                                        self.current_idx[head_name + utils.SPLIT_OBJ_HEAD] += 1
                                        print('current color id: ', self.current_idx[head_name + utils.SPLIT_OBJ_HEAD],
                                              'for class: ', head_name + utils.SPLIT_OBJ_HEAD)
                                        assert self.current_idx[head_name + utils.SPLIT_OBJ_HEAD] <= \
                                               self.segmentation_config[head_name + utils.SPLIT_OBJ_HEAD][
                                                   "max"], "ID pass out of range"
                                else:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[prefix + utils.SPLIT_OBJ_HEAD]
                                    self.current_idx[prefix + utils.SPLIT_OBJ_HEAD] += 1
                                    print('current color id: ', self.current_idx[prefix + utils.SPLIT_OBJ_HEAD],
                                          'for class: ', prefix + utils.SPLIT_OBJ_HEAD)
                                    assert self.current_idx[prefix + utils.SPLIT_OBJ_HEAD] <= \
                                           self.segmentation_config[prefix + utils.SPLIT_OBJ_HEAD][
                                               "max"], "ID pass out of range"
                                pass
                            else: # it is the other part (handle) of the splittable object
                                if prefix in utils.OBJECTS_WITH_SMALL_HANDLES:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[utils.SMALL_HANDLE_NAME]
                                    self.current_idx[utils.SMALL_HANDLE_NAME] += 1
                                    print('current color id: ', self.current_idx[utils.SMALL_HANDLE_NAME],
                                          'for class: ', utils.SMALL_HANDLE_NAME)
                                    assert self.current_idx[utils.SMALL_HANDLE_NAME] <= \
                                           self.segmentation_config[utils.SMALL_HANDLE_NAME][
                                               "max"], "ID pass out of range"
                                elif prefix in utils.OBJECTS_WITH_LONG_HANDLES:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[utils.LONG_HANDLE_NAME]
                                    self.current_idx[utils.LONG_HANDLE_NAME] += 1
                                    print('current color id: ', self.current_idx[utils.LONG_HANDLE_NAME],
                                          'for class: ', utils.LONG_HANDLE_NAME)
                                    assert self.current_idx[utils.LONG_HANDLE_NAME] <= \
                                           self.segmentation_config[utils.LONG_HANDLE_NAME][
                                               "max"], "ID pass out of range"
                                else:
                                    bpy.data.objects[o.name].pass_index = self.current_idx[prefix + utils.SPLIT_OBJ_HANDLE]
                                    self.current_idx[prefix + utils.SPLIT_OBJ_HANDLE] += 1
                                    print('current color id: ', self.current_idx[prefix + utils.SPLIT_OBJ_HANDLE],
                                          'for class: ', prefix + utils.SPLIT_OBJ_HANDLE)
                                    assert self.current_idx[prefix + utils.SPLIT_OBJ_HANDLE] <= \
                                           self.segmentation_config[prefix + utils.SPLIT_OBJ_HANDLE][
                                               "max"], "ID pass out of range"
                                pass

                        bpy.ops.object.mode_set(mode='OBJECT')
                        # important to continue, because we dont want split object masks to be overlayed by a mask of the whole object
                        continue

                    bpy.data.objects[item].pass_index = self.current_idx[prefix]
                    self.current_idx[prefix] += 1
                    print('current color id: ', self.current_idx[prefix], 'for class: ', prefix)
                    assert self.current_idx[prefix] <= self.segmentation_config[prefix]["max"], "ID pass out of range"
                    pass
            pass
            print("---------------------")
        print("#########################3")
        pass

    def render_sample(self):
        cameras_list = [obj.name for obj in bpy.context.scene.objects if 'Camera' in obj.name]

        for camera in cameras_list:
            bpy.context.scene.camera = bpy.context.scene.objects[camera]
            current_timestamp = str(time.time())
            bpy.data.scenes["Scene"].node_tree.nodes['File Output'].file_slots[0].path = "nat_" + current_timestamp
            if utils.RENDER_MASKS:
                bpy.data.scenes["Scene"].node_tree.nodes['File Output'].file_slots[1].path = "seg_" + current_timestamp
            if utils.GENERATE_DEPTH:
                bpy.data.scenes["Scene"].node_tree.nodes['File Output'].file_slots[-1].path = "dep_" + current_timestamp

            bpy.ops.render.render()

            self.current_idx = {}
            for key in self.segmentation_config:
                self.current_idx[key] = self.segmentation_config[key]["min"]

            pass

    def create_compositing_node_tree(self):
        bpy.context.scene.view_layers["View Layer"].use_pass_object_index = True

        if not bpy.context.scene.use_nodes:
            bpy.context.scene.use_nodes = True
            pass

        node_tree = bpy.context.scene.node_tree

        for current_node in node_tree.nodes:
            node_tree.nodes.remove(current_node)

        render_layers_node = node_tree.nodes.new("CompositorNodeRLayers")
        render_layers_node.location.x = -200
        render_layers_node.location.y = 400

        composite_node = node_tree.nodes.new("CompositorNodeComposite")
        composite_node.location.x = 600
        composite_node.location.y = 400

        math_node = node_tree.nodes.new("CompositorNodeMath")
        math_node.location.x = 200
        math_node.location.y = 100
        math_node.operation = "DIVIDE"
        math_node.inputs[1].default_value = 65536

        file_output_node = node_tree.nodes.new("CompositorNodeOutputFile")
        file_output_node.location.x = 600
        file_output_node.location.y = 100
        file_output_node.file_slots.remove(file_output_node.inputs[0])
        file_output_node.base_path = self.dataset_destination_path

        normalize_node = node_tree.nodes.new("CompositorNodeNormalize")
        normalize_node.location.x = 400
        normalize_node.location.y = 0


        file_output_node.file_slots.new("NaturalPhoto")
        if utils.RENDER_MASKS:
            file_output_node.file_slots.new("SegmentationPhoto")
            node_tree.nodes['File Output'].file_slots['SegmentationPhoto'].use_node_format = False
            node_tree.nodes['File Output'].file_slots['SegmentationPhoto'].format.color_mode = 'BW'
            node_tree.nodes['File Output'].file_slots['SegmentationPhoto'].format.color_depth = '16'
            node_tree.nodes['File Output'].file_slots['SegmentationPhoto'].format.compression = 0
            node_tree.links.new(math_node.outputs[0], file_output_node.inputs["SegmentationPhoto"])

        if utils.GENERATE_DEPTH:
            file_output_node.file_slots.new("DepthPhoto")
            node_tree.nodes['File Output'].file_slots['DepthPhoto'].use_node_format = False
            node_tree.nodes['File Output'].file_slots['DepthPhoto'].format.color_mode = 'BW'
            node_tree.nodes['File Output'].file_slots['DepthPhoto'].format.color_depth = '8'
            node_tree.nodes['File Output'].file_slots['DepthPhoto'].format.compression = 0
            node_tree.links.new(render_layers_node.outputs["Depth"], normalize_node.inputs[0])
            node_tree.links.new(normalize_node.outputs[0], file_output_node.inputs["DepthPhoto"])

        node_tree.links.new(render_layers_node.outputs["IndexOB"], math_node.inputs[0])
        node_tree.links.new(render_layers_node.outputs["Image"], composite_node.inputs["Image"])
        node_tree.links.new(render_layers_node.outputs["Image"], file_output_node.inputs["NaturalPhoto"])
        pass
