import abc
import math
import pprint
import random
import copy
import bpy

from . import drag_panel_op
from . import object_manipulator
from . import physics_mainpulator
from . import spawner
from . import utils
from .bl_ui_button import BL_UI_Button
from .bl_ui_checkbox import BL_UI_Checkbox
from .bl_ui_label import BL_UI_Label
from .bl_ui_textbox import BL_UI_Textbox
from .utils import Utils


class Policy:
    def __init__(self, region):
        self.region = region
        self.name = ''
        self.policy_checkbox_cat_list = list()
        self.description = BL_UI_Label(0, 0, 90, 15)
        self.selected_checkboxes = list()
        self.checkbox_start_x = 270
        self.checkbox_start_y = 340
        self.box_width = 100
        self.box_height = 15
        self.policy_attrs_in_main_panel = list()
        self.max_checkbox_col_row_count = None
        self.input_textbox_count = 0
        self.checkbox_column_count = None
        self.remaining_pictures_per_policy = None
        self.pictures_per_policy = None
        self.policy_active = False
        self.hidden_set_active_button_state = None
        self.decription = ''
        self.active_policy_button = ''
        self.policy_button_y = 0
        self.classes_dict = None
        self.amount_of_objects_in_sample = 5
        self.previous_pics_per_policy_ammount = 0
        pass

    @abc.abstractmethod
    def apply_post_physics(self):
        pass

    @abc.abstractmethod
    def apply_post_items(self):
        pass

    @abc.abstractmethod
    def apply(self):
        pass

    @abc.abstractmethod
    def add_samples_per_policy_input(self, main_panel):
        pass

    @abc.abstractmethod
    def apply_policy_button_clicked(self, widget):
        pass

    def choose_classes_weighted_probability(self, list_to_apply_probability, probability_for_classes, apply_weights):
        if apply_weights:
            rand_float = random.uniform(0.0, 1.0)
            if rand_float < probability_for_classes and set(list_to_apply_probability).issubset(
                    set(list(self.classes_dict.keys()))):
                chose_class = random.choice(list_to_apply_probability)
            else:
                chose_class = random.choice(list(self.classes_dict.keys()))
        else:
            chose_class = random.choice(list(self.classes_dict.keys()))
        return chose_class

    def get_total_samples_to_generate_from_all_policies(self, main_panel):
        dataset_size = 0
        for policy in main_panel.pics_per_policy_dict:
            if main_panel.pics_per_policy_dict[policy]['policy_active']:
                dataset_size += main_panel.pics_per_policy_dict[policy]['samples']
        main_panel.total_samples_to_generate = dataset_size
        main_panel.iter_label.text = '0 of {}'.format(str(main_panel.total_samples_to_generate))

    def on_samples_ammount_textbox_change(self, widget, context, event):
        if not bpy.context.screen.is_animation_playing:
            main_panel = drag_panel_op.WIDGET_INSTANCE
            self.pictures_per_policy = int(widget.text)
            self.remaining_pictures_per_policy = int(widget.text)

            main_panel.pics_per_policy_dict[self.name] = {'samples': self.pictures_per_policy,
                                                          'policy_active': self.policy_active}
            self.get_total_samples_to_generate_from_all_policies(main_panel)

    def check_boundary_attrs_exist(self, attrs_list):
        main_panel = drag_panel_op.WIDGET_INSTANCE
        for attr in attrs_list:
            if hasattr(main_panel, attr):
                return True
        return False

    def set_boundaries_widgets(self, set_active_btn_name):
        main_panel = drag_panel_op.WIDGET_INSTANCE
        x = main_panel.buttons_start_x + 190
        y = main_panel.buttons_start_y + 190
        set_active_btn_y = getattr(main_panel, set_active_btn_name).y
        set_active_btn_x = getattr(main_panel, set_active_btn_name).x
        set_active_button_name = 'active_policy_button_' + self.name

        width = self.max_checkbox_col_row_count * 55 if self.max_checkbox_col_row_count * 55 > getattr(main_panel,
                                                                                                       set_active_button_name).x - 160 else getattr(
            main_panel, set_active_button_name).x - 160

        width = set_active_btn_x - 110
        print('self.max_checkbox_col_row_count: ', self.max_checkbox_col_row_count)
        print('Active button name x: ', getattr(main_panel, set_active_button_name).x)
        height = set_active_btn_y + 35

        attr_list = ['upper_line_horizontal', 'lower_line_horizontal', 'right_line_vertical',
                     'left_upper_horizontal', 'left_lower_horizontal', 'left_lower_vertical', 'left_upper_vertical']
        if self.check_boundary_attrs_exist(attr_list):

            getattr(main_panel, 'upper_line_horizontal').x = x
            getattr(main_panel, 'upper_line_horizontal').y = y
            getattr(main_panel, 'upper_line_horizontal').width = width
            getattr(main_panel, 'upper_line_horizontal').height = 1

            getattr(main_panel, 'lower_line_horizontal').x = x
            getattr(main_panel, 'lower_line_horizontal').y = height
            getattr(main_panel, 'lower_line_horizontal').width = width
            getattr(main_panel, 'lower_line_horizontal').height = 1

            getattr(main_panel, 'right_line_vertical').x = x + width
            getattr(main_panel, 'right_line_vertical').y = y
            getattr(main_panel, 'right_line_vertical').width = 1
            getattr(main_panel, 'right_line_vertical').height = height - y + 1

            getattr(main_panel, 'left_upper_horizontal').x = x - 30
            getattr(main_panel, 'left_upper_horizontal').y = self.policy_button_y
            getattr(main_panel, 'left_upper_horizontal').width = 30
            getattr(main_panel, 'left_upper_horizontal').height = 1

            getattr(main_panel, 'left_lower_horizontal').x = x - 30
            getattr(main_panel, 'left_lower_horizontal').y = self.policy_button_y + 29
            getattr(main_panel, 'left_lower_horizontal').width = 30
            getattr(main_panel, 'left_lower_horizontal').height = 1

            getattr(main_panel, 'left_lower_vertical').x = x
            getattr(main_panel, 'left_lower_vertical').y = self.policy_button_y + 29
            getattr(main_panel, 'left_lower_vertical').width = 1
            getattr(main_panel, 'left_lower_vertical').height = height - self.policy_button_y - 29

            getattr(main_panel, 'left_upper_vertical').x = x
            getattr(main_panel, 'left_upper_vertical').y = y
            getattr(main_panel, 'left_upper_vertical').width = 1
            getattr(main_panel, 'left_upper_vertical').height = self.policy_button_y - y + 1
            pass

        else:

            upper_line_horizontal = BL_UI_Button(x, y, width, 1)
            upper_line_horizontal.disabled = True
            upper_line_horizontal.text = ''

            lower_line_horizontal = BL_UI_Button(x, height, width, 1)
            lower_line_horizontal.disabled = True
            lower_line_horizontal.text = ''

            right_line_vertical = BL_UI_Button(x + width, y, 1, height - y + 1)
            right_line_vertical.disabled = True
            right_line_vertical.text = ''

            left_upper_horizontal = BL_UI_Button(x - 30, self.policy_button_y, 30, 1)
            left_upper_horizontal.disabled = True
            left_upper_horizontal.text = ''

            left_lower_horizontal = BL_UI_Button(x - 30, self.policy_button_y + 29, 30, 1)
            left_lower_horizontal.disabled = True
            left_lower_horizontal.text = ''

            left_lower_vertical = BL_UI_Button(x, self.policy_button_y + 29, 1, height - self.policy_button_y - 29)
            left_lower_vertical.disabled = True
            left_lower_vertical.text = ''

            left_upper_vertical = BL_UI_Button(x, y, 1, self.policy_button_y - y + 1)
            left_upper_vertical.disabled = True
            left_upper_vertical.text = ''

            setattr(main_panel, 'upper_line_horizontal', upper_line_horizontal)
            setattr(main_panel, 'lower_line_horizontal', lower_line_horizontal)
            setattr(main_panel, 'right_line_vertical', right_line_vertical)
            setattr(main_panel, 'left_upper_horizontal', left_upper_horizontal)
            setattr(main_panel, 'left_lower_horizontal', left_lower_horizontal)
            setattr(main_panel, 'left_lower_vertical', left_lower_vertical)
            setattr(main_panel, 'left_upper_vertical', left_upper_vertical)

        return attr_list

    # def get_checked_policy_boxes_attr_names(self):
    #     list_of_checked_arrt_names = list()
    #     for box_category in self.policy_checkbox_cat_list:
    #         for attr_name in box_category:
    #             if getattr(drag_panel_op.WIDGET_INSTANCE, attr_name).is_checked():
    #                 list_of_checked_arrt_names.append(attr_name)
    #     return list_of_checked_arrt_names

    def get_checked_classes(self, main_panel):
        ret_list = list()
        attr_list = dir(main_panel)
        for attr in attr_list:
            if self.name in attr and type(getattr(main_panel, attr)) == BL_UI_Checkbox:
                if getattr(main_panel, attr).is_checked:
                    class_name = attr.split('_')[0]
                    ret_list.append(class_name)
        return ret_list

    def get_active_policies(self):
        '''
            This method has to be called at the end of apply_policy_button_clicked
        '''
        ret_list = list()
        policies_dict = drag_panel_op.WIDGET_INSTANCE.policies_dict
        print("get_active_policies, policies_dict: ", policies_dict)
        for policy_key in policies_dict:
            policy_instance = policies_dict[policy_key]['instance']
            if policy_instance and policy_instance.policy_active:
                ret_list.append(policy_instance.name)
        print("ACTIVE POLICIES: ", str(ret_list))
        drag_panel_op.WIDGET_INSTANCE.applied_policies_label.text = 'Applied policies: ' + ', '.join(
            [str(elem) for elem in ret_list])

        return ret_list

    def draw_policy(self, main_panel, attrs_present=False):
        base_widgets = main_panel.get_base_widget_list()
        # add policy buttons to redraw
        policy_button_attr_list = main_panel.get_policy_buttons_attr_names()

        if not attrs_present:
            policy_input_boxes = self.add_samples_per_policy_input(main_panel)
            added_attrs = self.add_policy_checkboxes(main_panel)
            self.active_policy_button = self.add_set_active_button(main_panel)
            added_attrs.append(self.active_policy_button)
            for input_box_attr_name in policy_input_boxes:
                added_attrs.append(input_box_attr_name)

        else:
            added_attrs = self.policy_attrs_in_main_panel

        for attr_name in added_attrs:
            base_widgets.append(getattr(main_panel, attr_name))
        for policy_button_attr in policy_button_attr_list:
            base_widgets.append(getattr(main_panel, policy_button_attr))

        new_lower_widget_bound = self.max_checkbox_col_row_count * 40 + self.input_textbox_count * 25 + 245
        new_panel_height = new_lower_widget_bound + main_panel.lower_bound_panel_height_diff
        # print("new lower widget bound: ", new_lower_widget_bound)
        # print("main_panel.lower_widget_bound: ", main_panel.lower_widget_bound)
        main_panel.lower_widget_bound = main_panel.lower_widget_bound if new_lower_widget_bound < main_panel.lower_widget_bound else new_lower_widget_bound
        main_panel.panel.height = main_panel.panel.height if new_panel_height < main_panel.panel.height else new_panel_height

        for line in self.set_boundaries_widgets(self.active_policy_button):
            base_widgets.append(getattr(main_panel, line))

        base_widgets.append(getattr(main_panel, 'generate_masks_box'))
        base_widgets.append(getattr(main_panel, 'generate_depth_box'))

        widgets = [main_panel.panel]
        widgets += base_widgets

        main_panel.spawn_bottom_buttons(main_panel.lower_widget_bound)
        main_panel.init_widgets(bpy.context, widgets)
        main_panel.panel.add_widgets(base_widgets)

    def set_checkbox_attrs_names_to_add(self):
        # for each cat name, category path tuple in CAT list
        for model_class_path in utils.CAT_LIST:
            # print("CURRENT CAT PATH: ", model_class_path)
            # set list attr to hold class names for each category
            setattr(self, model_class_path[0], list())
            # for class in category set a checkbox attr and append to list of cat attrs
            for cls in utils.Utils.list_class_dirs(model_class_path[1]):
                class_name = cls.split('/')[-2:][0]
                attribute_to_add = class_name + '_' + self.name
                getattr(self, model_class_path[0]).append(attribute_to_add)
            # append a list of cat checkbox attr to a list aof category arrts list
            self.policy_checkbox_cat_list.append(getattr(self, model_class_path[0]))

    def check_main_widget_has_policy_attrs(self, main_panel):
        for cat_checkbox_list in self.policy_checkbox_cat_list:
            for checkbox_attr_name in cat_checkbox_list:
                if hasattr(main_panel, checkbox_attr_name):
                    pass
                else:
                    return False
        return True

    def set_checkbox_enabled_for_spawn_in_containers_policy(self, spacial_policy_list, checkbox_attr_name,
                                                            current_category_no):
        containers_col_no = utils.CONTAINERS_CATEGORY_INDEX
        if any(policy_name in checkbox_attr_name for policy_name in spacial_policy_list):
            if current_category_no == containers_col_no:
                return True
            else:
                return False
        else:
            return True

    def set_toggle_action_depending_on_policy(self, policy_name):
        if policy_name in utils.SPAWN_IN_CONTAINERS_POLICIES:
            return self.spawn_in_containers_policy_class_checkbox_callback
        else:
            return self.policy_class_checkbox_callback

    def set_toggle_select_all(self, widget):
        print('WIDGET STATE: ', widget.state)
        print('policy_attrs_in_main_panel : ', self.policy_attrs_in_main_panel)
        print(self.name)
        for attr in self.policy_attrs_in_main_panel:
            print(attr)
            if self.name in attr and type(getattr(drag_panel_op.WIDGET_INSTANCE, attr)) == BL_UI_Checkbox:
                if getattr(drag_panel_op.WIDGET_INSTANCE, attr).group == 'policy':

                    getattr(drag_panel_op.WIDGET_INSTANCE, attr).state = widget.state

    def add_policy_checkboxes(self, main_panel):
        added_attributes = list()
        current_cat_no = 0
        max_checkbox_column_row_count = 0
        spawn_in_container_policy_list = utils.SPAWN_IN_CONTAINERS_POLICIES
        for cat_checkbox_list in self.policy_checkbox_cat_list:
            current_box_no = 1
            for checkbox_attr_name in cat_checkbox_list:
                setattr(main_panel, checkbox_attr_name, BL_UI_Checkbox(self.checkbox_start_x + current_cat_no * 160,
                                                                       self.checkbox_start_y + current_box_no * 25 + 25,
                                                                       self.box_width, self.box_height, 'policy'))
                getattr(main_panel, checkbox_attr_name).text = checkbox_attr_name.split('_')[0]
                getattr(main_panel, checkbox_attr_name).text_size = 14
                getattr(main_panel, checkbox_attr_name).text_color = (0.2, 0.9, 0.9, 1.0)
                getattr(main_panel,
                        checkbox_attr_name).enabled = self.set_checkbox_enabled_for_spawn_in_containers_policy(
                    spawn_in_container_policy_list, checkbox_attr_name, current_cat_no)
                getattr(main_panel, checkbox_attr_name).set_toggle_action(
                    self.set_toggle_action_depending_on_policy(self.name))

                added_attributes.append(checkbox_attr_name)
                current_box_no += 1

            if current_cat_no == 0:
                setattr(main_panel, 'select_all_box', BL_UI_Checkbox(self.checkbox_start_x + current_cat_no * 160,
                                                                     self.checkbox_start_y + current_box_no * 25 + 25,
                                                                     self.box_width, self.box_height, 'select_all'))
                getattr(main_panel, 'select_all_box').text = 'SELECT ALL'
                getattr(main_panel, 'select_all_box').text_size = 14
                getattr(main_panel, 'select_all_box').text_color = (0.2, 0.9, 0.9, 1.0)
                getattr(main_panel, 'select_all_box').enabled = True
                getattr(main_panel, 'select_all_box').set_toggle_action(self.set_toggle_select_all)
                added_attributes.append('select_all_box')
            max_checkbox_column_row_count = max_checkbox_column_row_count if current_box_no < max_checkbox_column_row_count else current_box_no
            current_box_no = 1
            current_cat_no += 1
        self.policy_attrs_in_main_panel = added_attributes
        # print("ATTRS TO ADD TO MAIN PANEL: ", added_attributes)
        self.max_checkbox_col_row_count = max_checkbox_column_row_count
        self.checkbox_column_count = current_cat_no - 1
        return added_attributes

    def spawn_in_containers_policy_class_checkbox_callback(self, widget):
        set_active_button_name = 'active_policy_button_' + self.name
        # putting together lists of class names in categories into a single list of class names
        containers_classes = self.policy_checkbox_cat_list[utils.CONTAINERS_CATEGORY_INDEX]
        policy_checkbox_list = [inner for outer in self.policy_checkbox_cat_list for inner in outer]
        main_panel = drag_panel_op.WIDGET_INSTANCE
        all_but_containers_classes_attrs = [x for x in policy_checkbox_list if x not in containers_classes]

        if any(getattr(main_panel, container_class_attr).is_checked for container_class_attr in containers_classes):
            for attr_name in all_but_containers_classes_attrs:
                getattr(main_panel, attr_name).enabled = True
            getattr(main_panel, set_active_button_name).disabled = False
        else:
            for attr_name in all_but_containers_classes_attrs:
                getattr(main_panel, attr_name).enabled = False
                getattr(main_panel, attr_name).state = False

            getattr(main_panel, set_active_button_name).disabled = True

    def policy_class_checkbox_callback(self, widget):
        set_active_button_name = 'active_policy_button_' + self.name
        print("set_active_button_name: ", set_active_button_name)
        policy_checkbox_list = [inner for outer in self.policy_checkbox_cat_list for inner in outer]
        main_panel = drag_panel_op.WIDGET_INSTANCE

        for checkbox in policy_checkbox_list:

            if getattr(main_panel, checkbox).is_checked:
                getattr(main_panel, set_active_button_name).disabled = False
                return
        getattr(main_panel, set_active_button_name).disabled = True

    def toogle_set_active_button(self, main_panel, toogle_val):
        button_name = 'active_policy_button_' + self.name
        getattr(main_panel, button_name).disabled = toogle_val

    def add_set_active_button(self, main_panel):
        button_name = 'active_policy_button_' + self.name
        setattr(main_panel, button_name, BL_UI_Button(self.checkbox_start_x + self.checkbox_column_count * 160,
                                                      self.checkbox_start_y + self.max_checkbox_col_row_count * 25 + 35,
                                                      90, 30))
        getattr(main_panel, button_name).disabled = True
        getattr(main_panel, button_name).bg_color = (0.2, 0.8, 0.8, 0.8)
        getattr(main_panel, button_name).hover_bg_color = (0.2, 0.9, 0.9, 1.0)
        getattr(main_panel, button_name).text = "Apply policy"
        getattr(main_panel, button_name).set_image_size((24, 24))
        getattr(main_panel, button_name).set_image_position((4, 2))
        getattr(main_panel, button_name).set_mouse_down(self.apply_policy_button_clicked)

        return button_name

    # def get_selected_classes(self, main_panel):
    #     checked_classes = list()
    #     for cat_checkbox_list in self.policy_checkbox_cat_list:
    #         for checkbox_attr_name in cat_checkbox_list:
    #             if getattr(main_panel, checkbox_attr_name).is_checked():
    #                 checked_classes.append(checkbox_attr_name.split('_')[0])
    #     self.selected_checkboxes = checked_classes
    #     return checked_classes


class SingleWithoutSpacing(Policy):

    # POLICY 1
    def __init__(self, region):
        super().__init__(region)
        self.samples_textbox_name = ''
        self.objects_per_sapwn_textbox_name = ''
        self.checkbox_start_y = 310
        self.set_containers_physics = True
        self.decription = 'Description: Generate selected class items in random places'

    def apply_post_physics(self):
        pass

    def apply_post_items(self):
        pass

    def add_samples_per_policy_input(self, main_panel):

        description_label_name = self.name + '_description'
        label_y = main_panel.buttons_start_y + 190
        setattr(main_panel, description_label_name, BL_UI_Label(self.checkbox_start_x, label_y + 3, 90, 15))
        getattr(main_panel, description_label_name).text = self.decription
        getattr(main_panel, description_label_name).text_size = 14
        getattr(main_panel, description_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        label_name = 'set_number_label_' + self.name
        self.samples_textbox_name = textbox_name = 'input_box_' + self.name
        setattr(main_panel, label_name, BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 0, 90, 15))
        getattr(main_panel, label_name).text = 'Samples to generate:'
        getattr(main_panel, label_name).text_size = 14
        getattr(main_panel, label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 3, 145, 30))
        getattr(main_panel, textbox_name).text = '100'
        getattr(main_panel, textbox_name).set_text_changed(self.on_samples_ammount_textbox_change)

        objects_per_sapwn_label_name = 'set_objects_per_sapwn_' + self.name
        self.objects_per_sapwn_textbox_name = objects_textbox_name = 'objects_input_box_' + self.name
        setattr(main_panel, objects_per_sapwn_label_name,
                BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 40, 90, 15))
        getattr(main_panel, objects_per_sapwn_label_name).text = 'Objects per sample:'
        getattr(main_panel, objects_per_sapwn_label_name).text_size = 14
        getattr(main_panel, objects_per_sapwn_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, objects_textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 43, 145, 30))
        getattr(main_panel, objects_textbox_name).text = '5'

        self.policy_attrs_in_main_panel.append(label_name)
        self.policy_attrs_in_main_panel.append(textbox_name)
        self.policy_attrs_in_main_panel.append(objects_per_sapwn_label_name)
        self.policy_attrs_in_main_panel.append(objects_textbox_name)

        return [label_name, textbox_name, description_label_name, objects_per_sapwn_label_name, objects_textbox_name]

    def apply_policy_button_clicked(self, widget):
        self.previous_pics_per_policy_ammount = copy.deepcopy(self.pictures_per_policy)
        self.input_textbox_count = 2
        self.pictures_per_policy = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.samples_textbox_name).text)
        self.remaining_pictures_per_policy = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.samples_textbox_name).text)
        self.amount_of_objects_in_sample = int(
            getattr(drag_panel_op.WIDGET_INSTANCE, self.objects_per_sapwn_textbox_name).text)
        if not self.policy_active:
            drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate += self.pictures_per_policy
            drag_panel_op.WIDGET_INSTANCE.iter_label.text = '0 of {}'.format(
                str(drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate))
            self.policy_active = True
            main_panel = drag_panel_op.WIDGET_INSTANCE
            main_panel.pics_per_policy_dict[self.name]['policy_active'] = self.policy_active
        active_policies_list = self.get_active_policies()

        if active_policies_list:
            drag_panel_op.WIDGET_INSTANCE.button1.disabled = False

    def apply(self):
        widget = drag_panel_op.WIDGET_INSTANCE
        if self.classes_dict is None:
            self.classes_dict = utils.Utils.get_classes_models_paths_dict(widget, self.get_checked_classes(widget))

        sp = spawner.Spawner(self.region.origin, self.region.diagonal)
        chose_models = []

        for i in range(self.amount_of_objects_in_sample):
            print("classes dict: ", list(self.classes_dict.keys()))

            chose_class = self.choose_classes_weighted_probability(utils.WEIGHTED_CLASS_CHOICE,
                                                                   utils.WEIGHTED_SPAWNING_THRESHOLD,
                                                                   utils.WEIGHTED_SPAWNING)
            # if utils.WEIGHTED_SPAWNING:
            #     rand_float = random.uniform(0.0, 1.0)
            #     if rand_float < utils.WEIGHTED_SPAWNING_THRESHOLD and utils.WEIGHTED_CLASS_CHOICE in list(self.classes_dict.keys()):
            #         chose_class = utils.WEIGHTED_CLASS_CHOICE
            #     else:
            #         chose_class = random.choice(list(self.classes_dict.keys()))
            # else:
            #     chose_class = random.choice(list(self.classes_dict.keys()))

            chose_model = random.choice(self.classes_dict[chose_class])
            chose_models.append(chose_model)

        print(chose_models)
        sp.spawn_items_from_list_with_random_position(chose_models, self.set_containers_physics)
        pass


class ItemsGroup(Policy):
    def apply_post_physics(self):
        pass

    def apply_post_items(self):
        pass

    def add_samples_per_policy_input(self, main_panel):
        pass

    def apply_policy_button_clicked(self, widget):
        pass

    def __init__(self, region, amount, classes_dict):
        super().__init__(region)
        assert region.origin == region.diagonal, "ItemsGroup Policy requires single point as region"
        self.amount_of_objects_in_sample = amount
        self.classes_dict = classes_dict

    def apply(self):
        region_around_point = self.region
        region_around_point.origin.x -= 0.05
        region_around_point.origin.y -= 0.05
        region_around_point.diagonal.x += 0.05
        region_around_point.diagonal.y += 0.05
        single_model_policy = SingleWithoutSpacing(region_around_point)
        single_model_policy.amount_of_objects_in_sample = self.amount_of_objects_in_sample
        single_model_policy.classes_dict = self.classes_dict
        single_model_policy.set_containers_physics = False
        single_model_policy.apply()
        pass


class ContainerWithItems(Policy):
    # POLICY 2
    def apply_post_physics(self):
        physics = physics_mainpulator.PhysicsManipulator()
        physics.set_containers_properties()
        pass

    def apply_post_items(self):
        scene = bpy.context.scene
        widget = drag_panel_op.WIDGET_INSTANCE
        checked_consumables_items = utils.Utils.get_classes_models_paths_dict(widget, self.get_checked_classes(widget),
                                                                              "Consumables")
        if not checked_consumables_items:
            print("There is no consumables checked for spawning in containers")
            return
        else:
            print(list(checked_consumables_items.keys()))
        containers = [obj for obj in scene.objects if utils.WITH_CONE_POSTFIX in obj.name]
        for obj in containers:
            point = utils.Point(obj.matrix_world.to_translation().x, obj.matrix_world.to_translation().y, 0.1)
            item_group = ItemsGroup(utils.Area(point, point), self.no_objects,
                                    checked_consumables_items)
            item_group.apply()
        pass

    def __init__(self, region):
        super().__init__(region)
        self.no_obj_in_container_textbox_name = ''
        self.no_containers_textbox_name = ''
        self.no_samples_textbox_name = ''
        self.no_containers = 0
        self.no_objects = 0
        self.checkbox_start_y = 390
        self.decription = 'Description: Generate selected class items in selected containers'

        pass

    def apply(self):
        widget = drag_panel_op.WIDGET_INSTANCE
        containers_dict = utils.Utils.get_classes_models_paths_dict(widget, self.get_checked_classes(widget),
                                                                    "Containers")
        sp = spawner.Spawner(self.region.origin, self.region.diagonal)

        containers_paths_list = []
        for _ in range(self.no_containers):
            chose_class = random.choice(list(containers_dict.keys()))
            chose_model = random.choice(containers_dict[chose_class])
            containers_paths_list.append(chose_model)
        sp.spawn_items_from_list_with_random_position(containers_paths_list)
        pass

    def add_samples_per_policy_input(self, main_panel):
        self.input_textbox_count = 3

        description_label_name = self.name + '_description'
        label_y = main_panel.buttons_start_y + 190
        setattr(main_panel, description_label_name, BL_UI_Label(self.checkbox_start_x, label_y + 3, 90, 15))
        getattr(main_panel, description_label_name).text = self.decription
        getattr(main_panel, description_label_name).text_size = 14
        getattr(main_panel, description_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        label_name = 'set_number_label_' + self.name
        self.no_samples_textbox_name = textbox_name = 'input_box_' + self.name
        setattr(main_panel, label_name, BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 80, 90, 15))
        getattr(main_panel, label_name).text = 'Samples to generate:'
        getattr(main_panel, label_name).text_size = 14
        getattr(main_panel, label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 83, 145, 30))
        getattr(main_panel, textbox_name).text = '100'
        getattr(main_panel, textbox_name).set_text_changed(self.on_samples_ammount_textbox_change)

        cont_label_name = 'set_container_label_' + self.name
        self.no_containers_textbox_name = cont_textbox_name = 'container_input_box_' + self.name
        setattr(main_panel, cont_label_name, BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 40, 90, 15))
        getattr(main_panel, cont_label_name).text = 'Containers to spawn:'
        getattr(main_panel, cont_label_name).text_size = 14
        getattr(main_panel, cont_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, cont_textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 43, 145, 30))
        getattr(main_panel, cont_textbox_name).text = '5'

        obj_label_name = 'set_object_label_' + self.name
        self.no_obj_in_container_textbox_name = obj_textbox_name = 'object_input_box_' + self.name
        setattr(main_panel, obj_label_name, BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 0, 90, 15))
        getattr(main_panel, obj_label_name).text = 'Objects to spawn in each container :'
        getattr(main_panel, obj_label_name).text_size = 14
        getattr(main_panel, obj_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, obj_textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 3, 145, 30))
        getattr(main_panel, obj_textbox_name).text = '5'
        self.policy_attrs_in_main_panel.append(label_name)
        self.policy_attrs_in_main_panel.append(textbox_name)
        self.policy_attrs_in_main_panel.append(cont_label_name)
        self.policy_attrs_in_main_panel.append(cont_textbox_name)
        self.policy_attrs_in_main_panel.append(obj_label_name)
        self.policy_attrs_in_main_panel.append(obj_textbox_name)
        return [label_name, textbox_name, cont_label_name, cont_textbox_name, obj_label_name, obj_textbox_name,
                description_label_name]

    def apply_policy_button_clicked(self, widget):
        self.previous_pics_per_policy_ammount = copy.deepcopy(self.pictures_per_policy)
        self.pictures_per_policy = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.no_samples_textbox_name).text)
        self.no_containers = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.no_containers_textbox_name).text)
        self.no_objects = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.no_obj_in_container_textbox_name).text)
        self.remaining_pictures_per_policy = int(
            getattr(drag_panel_op.WIDGET_INSTANCE, self.no_samples_textbox_name).text)
        if not self.policy_active:
            drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate += self.pictures_per_policy
            drag_panel_op.WIDGET_INSTANCE.iter_label.text = '0 of {}'.format(
                str(drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate))
            self.policy_active = True
            main_panel = drag_panel_op.WIDGET_INSTANCE
            main_panel.pics_per_policy_dict[self.name]['policy_active'] = self.policy_active

        if not self.policy_active:
            drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate += self.pictures_per_policy
            drag_panel_op.WIDGET_INSTANCE.iter_label.text = '0 of {}'.format(
                str(drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate))
            self.policy_active = True
        active_policies_list = self.get_active_policies()
        if active_policies_list:
            drag_panel_op.WIDGET_INSTANCE.button1.disabled = False


class SingleWithSpacing(Policy):
    # Policy 3
    def __init__(self, region):
        super().__init__(region)
        self.padding_x = 0.1
        self.padding_y = 0.1
        self.checkbox_start_y = 310
        self.decription = 'Description: Generate evenly spaced items'
        self.utls = Utils()

    def apply_post_physics(self):
        pass

    def apply_post_items(self):
        pass

    def calculate_paddings(self, n, width, height):
        aspect_ratio = height / width
        x = math.ceil(math.sqrt((n / aspect_ratio)))
        y = math.ceil(aspect_ratio * x)

        padding_x = width / x
        padding_y = height / y

        return {
            "amount_x": x,
            "amount_y": y,
            "padding_x": padding_x,
            "padding_y": padding_y
        }

    def apply(self):
        n = self.amount_of_objects_in_sample
        x = abs(self.region.diagonal.x - self.region.origin.x)
        y = abs(self.region.diagonal.y - self.region.origin.y)

        paddings_info = self.calculate_paddings(n, x, y)

        widget = drag_panel_op.WIDGET_INSTANCE
        self.classes_dict = utils.Utils.get_classes_models_paths_dict(widget, self.get_checked_classes(widget))
        spwn = spawner.Spawner(self.region.origin, self.region.diagonal)
        scene = bpy.context.scene
        physics = physics_mainpulator.PhysicsManipulator()
        obj_manip = object_manipulator.ObjectManipulator()

        for _ in range(self.amount_of_objects_in_sample):
            chose_class = self.choose_classes_weighted_probability(utils.WEIGHTED_CLASS_CHOICE,
                                                                   utils.WEIGHTED_SPAWNING_THRESHOLD,
                                                                   utils.WEIGHTED_SPAWNING)

            # if utils.WEIGHTED_SPAWNING:
            #     rand_float = random.uniform(0.0, 1.0)
            #     if rand_float < utils.WEIGHTED_SPAWNING_THRESHOLD and utils.WEIGHTED_CLASS_CHOICE in list(self.classes_dict.keys()):
            #         chose_class = utils.WEIGHTED_CLASS_CHOICE
            #     else:
            #         chose_class = random.choice(list(self.classes_dict.keys()))
            # else:
            #     chose_class = random.choice(list(self.classes_dict.keys()))
            # chose_class = random.choice(list(self.classes_dict.keys()))
            chose_model = random.choice(self.classes_dict[chose_class])
            with_cone = True if chose_class in utils.CLASSES_WITH_CONE else False
            spwn.spawn_object(chose_model, chose_class, with_cone=with_cone)

        obj_manip.set_all_object_origin_to_geometry()

        cell_x = 1
        cell_y = 1
        current_x = self.region.origin.x + paddings_info["padding_x"] / 2
        print("current_x", current_x)
        current_y = self.region.origin.y + paddings_info["padding_y"] / 2
        print("current_y", current_y)
        pprint.pprint(paddings_info)
        for obj in scene.objects:
            if obj.type != 'MESH' or utils.SPAWNED_OBJ_POSTFIX not in obj.name:
                continue

            if cell_x > paddings_info["amount_x"]:
                cell_x = 1
                current_x = self.region.origin.x + paddings_info["padding_y"] / 2
                cell_y += 1
                current_y += paddings_info["padding_y"]
                print("cell_y", cell_y)
                assert cell_y <= paddings_info["amount_y"], "Something wrong with the padding"

            print("CELL_Y", cell_y)
            print("CELL_X", cell_x)
            print("current_x", current_x)
            print("current_y", current_y)
            print("-----------------------------------")
            obj.location[0] = current_x
            obj.location[1] = current_y
            obj.location[2] = 0.2

            cell_x += 1
            current_x += paddings_info["padding_x"]
            if utils.DONT_RANDOMIZE_POS in obj.name or utils.WITH_CONE_POSTFIX in obj.name:
                continue
            if utils.SPAWNED_OBJ_POSTFIX in obj.name:
                obj.rotation_euler = [self.utls.rand_float(0, 2 * math.pi),
                                      self.utls.rand_float(0, 2 * math.pi),
                                      self.utls.rand_float(0, 2 * math.pi)]

        physics.set_all_object_rigid_body_and_collision()
        # obj_manip.randomize_all_object_rotation()
        physics.set_all_objects_sticky_surface()
        pass

    def add_samples_per_policy_input(self, main_panel):

        description_label_name = self.name + '_description'
        label_y = main_panel.buttons_start_y + 190
        setattr(main_panel, description_label_name, BL_UI_Label(self.checkbox_start_x, label_y + 3, 90, 15))
        getattr(main_panel, description_label_name).text = self.decription
        getattr(main_panel, description_label_name).text_size = 14
        getattr(main_panel, description_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        label_name = 'set_number_label_' + self.name
        self.samples_textbox_name = textbox_name = 'input_box_' + self.name
        setattr(main_panel, label_name, BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 0, 90, 15))
        getattr(main_panel, label_name).text = 'Samples to generate:'
        getattr(main_panel, label_name).text_size = 14
        getattr(main_panel, label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 3, 145, 30))
        getattr(main_panel, textbox_name).text = '100'
        getattr(main_panel, textbox_name).set_text_changed(self.on_samples_ammount_textbox_change)

        objects_per_sapwn_label_name = 'set_objects_per_sapwn_' + self.name
        self.objects_per_sapwn_textbox_name = objects_textbox_name = 'objects_input_box_' + self.name
        setattr(main_panel, objects_per_sapwn_label_name,
                BL_UI_Label(self.checkbox_start_x, self.checkbox_start_y - 40, 90, 15))
        getattr(main_panel, objects_per_sapwn_label_name).text = 'Objects per sample:'
        getattr(main_panel, objects_per_sapwn_label_name).text_size = 14
        getattr(main_panel, objects_per_sapwn_label_name).text_color = (0.2, 0.9, 0.9, 1.0)

        setattr(main_panel, objects_textbox_name,
                BL_UI_Textbox(self.checkbox_start_x + 240, self.checkbox_start_y - 43, 145, 30))
        getattr(main_panel, objects_textbox_name).text = '5'

        self.policy_attrs_in_main_panel.append(label_name)
        self.policy_attrs_in_main_panel.append(textbox_name)
        self.policy_attrs_in_main_panel.append(objects_per_sapwn_label_name)
        self.policy_attrs_in_main_panel.append(objects_textbox_name)

        return [label_name, textbox_name, description_label_name, objects_per_sapwn_label_name, objects_textbox_name]

    def apply_policy_button_clicked(self, widget):
        self.previous_pics_per_policy_ammount = copy.deepcopy(self.pictures_per_policy)
        self.input_textbox_count = 2
        self.pictures_per_policy = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.samples_textbox_name).text)
        self.remaining_pictures_per_policy = int(getattr(drag_panel_op.WIDGET_INSTANCE, self.samples_textbox_name).text)
        self.amount_of_objects_in_sample = int(
            getattr(drag_panel_op.WIDGET_INSTANCE, self.objects_per_sapwn_textbox_name).text)
        print("APPLY CLICKED: self.policy_active - ", self.policy_active)
        if not self.policy_active:
            drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate += self.pictures_per_policy
            print("drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate: ",
                  drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate)
            drag_panel_op.WIDGET_INSTANCE.iter_label.text = '0 of {}'.format(
                str(drag_panel_op.WIDGET_INSTANCE.total_samples_to_generate))
            self.policy_active = True
            main_panel = drag_panel_op.WIDGET_INSTANCE
            main_panel.pics_per_policy_dict[self.name]['policy_active'] = self.policy_active
        active_policies_list = self.get_active_policies()
        print("APPLY CLICKED: active_policies_list - ", active_policies_list)

        if active_policies_list:
            drag_panel_op.WIDGET_INSTANCE.button1.disabled = False
