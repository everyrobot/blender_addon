bl_info = {
    "name": "Create Dataset",
    "blender": (2, 90, 1),
    "category": "All",
}

if "bpy" in locals():
    import importlib

    importlib.reload(object_mainpulator)
    importlib.reload(physics_manipulator)
    importlib.reload(spawner)
    importlib.reload(utils)
    importlib.reload(dataset_creator)
    importlib.reload(file_operator)

else:
    from . import object_manipulator
    from . import physics_mainpulator
    from . import spawner
    from . import utils
    from . import dataset_creator
    from . import file_operator

import bpy
from .drag_panel_op import DP_OT_draw_operator
from bpy.app.handlers import persistent

addon_keymaps = []

classes = (dataset_creator.CreateDataset,
           file_operator.ConfigFileOperator,
           file_operator.ModelsDirOperator,
           file_operator.OutputDirOperator,
           DP_OT_draw_operator)


@persistent
def load_handler():
    pass


def register():
    for c in classes:
        bpy.utils.register_class(c)

    #############
    # UI WIDGET #
    #############
    bpy.app.handlers.load_pre.append(load_handler)
    kcfg = bpy.context.window_manager.keyconfigs.addon
    if kcfg:
        km = kcfg.keymaps.new(name='3D View', space_type='VIEW_3D')

        kmi = km.keymap_items.new("object.dp_ot_draw_operator", 'F', 'PRESS', shift=True, ctrl=True)

        addon_keymaps.append((km, kmi))


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)

    #############
    # UI WIDGET #
    #############
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    for c in classes:
        bpy.utils.unregister_class(c)


# This allows you to run the script directly from Blender's Text editor
# to test the add-on without having to install it.
if __name__ == "__main__":
    register()
